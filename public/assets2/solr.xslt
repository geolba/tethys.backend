<?xml version="1.0" encoding="utf-8"?>
<!--
     /**
     * This file is part of TETHYS. The software TETHYS has been originally developed
     * at 
     *
     * LICENCE
     * TETHYS is free software; you can redistribute it and/or modify it under the
     * terms of the GNU General Public License as published by the Free Software
     * Foundation; either version 2 of the Licence, or any later version.
     * TETHYS is distributed in the hope that it will be useful, but WITHOUT ANY
     * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
     * details. You should have received a copy of the GNU General Public License 
     * along with TETHYS; if not, write to the Free Software Foundation, Inc., 51 
     * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
     *
     */
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:fn="http://example.com/functions"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:gmd="http://www.isotc211.org/2005/gmd"
                xmlns:gco="http://www.isotc211.org/2005/gco"  
                xmlns:json="http://www.w3.org/2013/XSL/json"              
                version="3.0">
    
    <xsl:output method="text"/>
    <!-- <xsl:mode on-no-match="shallow-copy"/>  -->
    
    <!-- Suppress output for all elements that don't have an explicit template. -->
    <!-- <xsl:template match="*" /> -->
    
    <!-- <xsl:output method="text"/> -->
    <!-- <xsl:mode on-no-match="shallow-copy"/>  -->
    
    <!-- Define a custom function to escape double quotes -->
    <xsl:function name="fn:escapeQuotes">
        <xsl:param name="input"/>
        <!-- <xsl:sequence select="translate($input, 'quot;', '`')"/> -->
        <xsl:sequence select="replace($input, '&quot;', '''')"/>
    </xsl:function>
    
    <xsl:template match="Rdr_Dataset">              
        
        <!-- <xsl:variable name="xml">
             <xsl:element name="field">
             <xsl:attribute name="name">id</xsl:attribute>
             <xsl:attribute name="value">
             <xsl:value-of select="@Id"/>
             </xsl:attribute>
             </xsl:element>
             </xsl:variable> -->
        
        <!-- OUTPUT -->
        <!-- Construct JSON directly -->
        <!-- <xsl:text>{"field": {"name": "id", "value": "</xsl:text>
             <xsl:value-of select="@Id"/>
             <xsl:text>"}}</xsl:text> -->
        <!-- Use CDATA to preserve curly braces -->
        
        <!-- <object>
             <string key="hello">world!</string>
             <number key="answer">42</number>
             <number key="lightspeed">3e8</number>
             <array key="urls">
             <string>http://example.com/</string>
             <string>http://example.org/</string>
             <string>http://example.net/</string>
             </array>
             </object> -->
        
        <!-- 1 id -->
        <xsl:text>{</xsl:text>
        <xsl:text>"id": "</xsl:text>
        <xsl:value-of select="@PublishId"/>
        <xsl:text>",</xsl:text>   
        
        <!-- 2 year --> 
        <xsl:variable name="year">
            <xsl:choose>
                <xsl:when test="ServerDatePublished/@Year != ''">
                    <xsl:value-of select="ServerDatePublished/@Year" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@PublishedYear" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>  
        <xsl:text>"year": "</xsl:text>
        <xsl:value-of select="$year"/>
        <xsl:text>",</xsl:text>     
        
        <!--3 year inverted -->
        <xsl:if test="$year">
            <xsl:variable name="yearInverted" select="65535 - $year"/>
            <xsl:text>"year_inverted": "</xsl:text>
            <xsl:value-of select="$yearInverted"/>           
            <xsl:text>",</xsl:text> 
        </xsl:if>
        
        <!--4 server_date_published -->
        <xsl:if test="ServerDatePublished/@UnixTimestamp != ''">           
            <xsl:text>"server_date_published": "</xsl:text> 
            <xsl:value-of select="ServerDatePublished/@UnixTimestamp" /> 
            <xsl:text>",</xsl:text>            
        </xsl:if>
        
        <!--5 server_date_modified -->
        <xsl:if test="ServerDateModified/@UnixTimestamp != ''">            
            <xsl:text>"server_date_modified": "</xsl:text>             
            <xsl:value-of select="/ServerDateModified/@UnixTimestamp" />    
            <xsl:text>",</xsl:text>         
        </xsl:if>
        
        <!--6 language -->
        <xsl:variable name="language" select="@Language" />
        <xsl:text>"language": "</xsl:text>
        <xsl:value-of select="$language"/>
        <xsl:text>",</xsl:text>
        
        <!--7 title / title_output -->
        <xsl:for-each select="TitleMain">
            <xsl:text>"title": "</xsl:text>
            <xsl:value-of select="fn:escapeQuotes(@Value)"/>
            <xsl:text>",</xsl:text>
            <xsl:if test="@Language = $language">
                <xsl:text>"title_output": "</xsl:text>
                <xsl:value-of select="@Value"/>
                <xsl:text>",</xsl:text>                
            </xsl:if>
        </xsl:for-each>
        
        <!--8 abstract / abstract_output -->
        <xsl:variable name="abstracts">
            <xsl:for-each select="TitleAbstract">
                <xsl:text>"</xsl:text>
                <!-- <xsl:value-of select="translate(@Value, '&quot;', '&apos;')" /> -->
                <xsl:value-of select="fn:escapeQuotes(@Value)"/>
                <xsl:text>"</xsl:text>
                <xsl:if test="position() != last()">,</xsl:if>
            </xsl:for-each>
        </xsl:variable> 
        <xsl:text>"abstract": [</xsl:text>
        <xsl:value-of select="$abstracts" />
        <xsl:text>],</xsl:text>
        
        <!-- <xsl:for-each select="TitleAbstract">            
             <xsl:if test="@Language = $language">
             <xsl:text>"abstract_output": "</xsl:text>              
             <xsl:value-of select="translate(@Value, '&quot;', '`')"/>
             <xsl:text>",</xsl:text>                
             </xsl:if>
             </xsl:for-each>       -->
        
        <!--9 author -->
        <xsl:variable name="authors">
            <xsl:for-each select="PersonAuthor">
                <xsl:sort select="@SortOrder"/>
                <xsl:text>"</xsl:text>
                <!-- <xsl:value-of select="translate(@Value, '&quot;', '`')"/> -->
                <xsl:value-of select="@LastName" />
                <xsl:text>, </xsl:text>
                <xsl:value-of select="@FirstName" />
                <xsl:text>"</xsl:text>
                <xsl:if test="position() != last()">,</xsl:if>
            </xsl:for-each> 
        </xsl:variable>
        <xsl:text>"author": [</xsl:text>
        <xsl:value-of select="$authors" />
        <xsl:text>],</xsl:text>
        <!-- <xsl:for-each select="PersonAuthor">
             <xsl:sort select="@SortOrder"/>
             <xsl:element name="field">
             <xsl:attribute name="name">author</xsl:attribute>
             <xsl:value-of select="@LastName" />
             <xsl:text>, </xsl:text>
             <xsl:value-of select="@FirstName" />
             </xsl:element>
             </xsl:for-each>                 -->
        
        <!-- author_sort -->
        <!-- <xsl:element name="field">
             <xsl:attribute name="name">author_sort</xsl:attribute>
             <xsl:for-each select="/Opus/Rdr_Dataset/PersonAuthor">
             <xsl:sort select="@SortOrder"/>
             <xsl:value-of select="@LastName" />
             <xsl:text></xsl:text>
             <xsl:value-of select="@FirstName" />
             <xsl:text></xsl:text>
             </xsl:for-each>
             </xsl:element> -->
        
        <!--16 doctype -->             
        <xsl:text>"doctype": "</xsl:text>
        <xsl:value-of select="@Type" />
        <xsl:text>",</xsl:text>  
        
        <!--17 +18 uncontrolled subject (swd) -->    
        <xsl:variable name="subjects">
            <xsl:for-each select="Subject[@Type = 'Uncontrolled']">
                <xsl:text>"</xsl:text>
                <xsl:value-of select="fn:escapeQuotes(@Value)"/>
                <xsl:text>"</xsl:text>
                <xsl:if test="position() != last()">,</xsl:if>
            </xsl:for-each>
        </xsl:variable>        
        <xsl:text>"subjects": [</xsl:text>
        <xsl:value-of select="$subjects" />
        <xsl:text>],</xsl:text>
        
        <!--19 belongs_to_bibliography -->        
        <xsl:text>"belongs_to_bibliography": </xsl:text>        
        <xsl:choose>
            <xsl:when test="@BelongsToBibliography = 1">
                <xsl:text>true</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>false</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>,</xsl:text>
        
        <!--21 title parent -->
        
        <!--22 title sub -->
        <xsl:if test="TitleSub">  
            <xsl:variable name="title_sub">
                <xsl:for-each select="TitleSub">
                    <xsl:text>"</xsl:text>
                    <xsl:value-of select="fn:escapeQuotes(@Value)"/>
                    <xsl:text>"</xsl:text>
                    <xsl:if test="position() != last()">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:for-each>
            </xsl:variable>       
            <xsl:text>"title_sub": [</xsl:text>
            <xsl:value-of select="$title_sub" />
            <xsl:text>],</xsl:text>
        </xsl:if>
        
        <!--23 title additional -->
        <xsl:variable name="title_additional">
            <xsl:for-each select="TitleAdditional">               
                <xsl:text>"</xsl:text>
                <!-- <xsl:value-of select="@Value"/> -->
                <xsl:value-of select="fn:escapeQuotes(@Value)"/>
                <xsl:text>"</xsl:text>
                <xsl:if test="position() != last()">
                    <xsl:text>, </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>  
        <xsl:text>"title_additional": [</xsl:text>
        <xsl:value-of select="string($title_additional)" />
        <xsl:text>],</xsl:text>
        
        <!--24 abstract additional -->
        <xsl:variable name="abstract_additional">
            <xsl:for-each select="TitleAbstractAdditional">
                <xsl:text>"</xsl:text>
                <xsl:value-of select="fn:escapeQuotes(@Value)"/>
                <xsl:text>"</xsl:text>
                <xsl:if test="position() != last()">
                    <xsl:text>, </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:text>"abstract_additional": [</xsl:text>
        <xsl:value-of select="$abstract_additional" />
        <xsl:text>],</xsl:text>      
        
        <!--25 licences -->
        <xsl:if test="Licence">
            <xsl:text>"licence": "</xsl:text>
            <xsl:value-of select="fn:escapeQuotes(Licence/@Name)" />
            <xsl:text>",</xsl:text>             
        </xsl:if>
        
        <!--27 creating corporation (single valued) -->
        <xsl:if test="@CreatingCorporation">            
            <xsl:text>"creating_corporation": "</xsl:text>
            <xsl:value-of select="@CreatingCorporation"/>
            <xsl:text>",</xsl:text> 
        </xsl:if>
        
        <!--28 contributing corporation (single valued) -->
        <xsl:if test="@ContributingCorporation">
            <xsl:text>"contributing_corporation": "</xsl:text>
            <xsl:value-of select="@ContributingCorporation"/>  
            <xsl:text>",</xsl:text>           
        </xsl:if>
        
        <!--29 publisher name (single valued) -->
        <xsl:if test="@PublisherName">          
            <xsl:text>"publisher_name": "</xsl:text>
            <xsl:value-of select="@PublisherName"/>
            <xsl:text>",</xsl:text>
        </xsl:if>
        
        <!--30 publisher place (single valued) -->
        <xsl:if test="@PublisherPlace">                   
            <xsl:text>"publisher_place": "</xsl:text>                     
            <xsl:value-of select="@PublisherPlace"/>  
            <xsl:text>",</xsl:text>                 
        </xsl:if>
        
        <!--31 publisher place (single valued) -->
        <xsl:if test="Coverage">
            <!-- In WKT format, use BBOX (minLon, maxLon, maxLat, minLat). -->
            <xsl:variable name="geolocation" select="concat(
                    Coverage/@XMin, ', ',  Coverage/@XMax, ', ', Coverage/@YMax, ', ', Coverage/@YMin)
                            " />
            <xsl:text>"geo_location": </xsl:text>            
            <xsl:text>"BBOX (</xsl:text>
            <xsl:value-of select="$geolocation"/>
            <xsl:text>)",</xsl:text> 
            
            <xsl:text>"bbox_xmin": </xsl:text>   
            <xsl:value-of select="Coverage/@XMin"/>
            <xsl:text>,</xsl:text> 
            <xsl:text>"bbox_xmax": </xsl:text>   
            <xsl:value-of select="Coverage/@XMax"/>
            <xsl:text>,</xsl:text> 
            <xsl:text>"bbox_ymin": </xsl:text>   
            <xsl:value-of select="Coverage/@YMin"/>
            <xsl:text>,</xsl:text> 
            <xsl:text>"bbox_ymax": </xsl:text>   
            <xsl:value-of select="Coverage/@YMax"/>
            <xsl:text>,</xsl:text> 
            
        </xsl:if>
        
        <!--32 identifier (multi valued) -->
        <xsl:variable name="identifier">
            <xsl:for-each select="Identifier">
                <xsl:text>"</xsl:text>
                <xsl:value-of select="fn:escapeQuotes(@Value)"/>
                <xsl:text>"</xsl:text>
                <xsl:if test="position() != last()">
                    <xsl:text>,</xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>       
        <xsl:text>"identifier": [</xsl:text>
        <xsl:value-of select="$identifier" />
        <xsl:text>],</xsl:text>        
        
        <!--33 reference (multi valued) -->
        <xsl:variable name="reference">
            <xsl:for-each select="Reference">
                <xsl:text>"</xsl:text>
                <xsl:value-of select="fn:escapeQuotes(@Value)"/>
                <xsl:text>"</xsl:text>
                <xsl:if test="position() != last()">
                    <xsl:text>,</xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>       
        <xsl:text>"reference": [</xsl:text>
        <xsl:value-of select="$reference" />
        <xsl:text>]</xsl:text>
        
        
        
        <xsl:text>}</xsl:text>       
    </xsl:template>
    
    <xsl:template match="/root2">
        <xsl:element name="add">
            <xsl:element name="doc">               
                
                
                
                
                
                
                
                <!--10 fulltext -->
                <xsl:for-each select="/Opus/Rdr_Dataset/Fulltext_Index">
                    <xsl:element name="field">
                        <xsl:attribute name="name">fulltext</xsl:attribute>
                        <xsl:value-of select="." />
                    </xsl:element>
                </xsl:for-each>
                
                <!--11 has fulltext -->
                <xsl:element name="field">
                    <xsl:attribute name="name">has_fulltext</xsl:attribute>
                    <xsl:value-of select="/Opus/Rdr_Dataset/Has_Fulltext" />
                </xsl:element>
                
                <!--12 IDs der Dateien, die mit nicht leerem Resultat extrahiert werden konnten -->
                <xsl:for-each select="/Opus/Rdr_Dataset/Fulltext_ID_Success">
                    <xsl:element name="field">
                        <xsl:attribute name="name">fulltext_id_success</xsl:attribute>
                        <xsl:value-of select="."/>
                    </xsl:element>
                </xsl:for-each>
                
                <!--13 IDs der Dateien, die nicht erfolgreich extrahiert werden konnten -->
                <xsl:for-each select="/Opus/Rdr_Dataset/Fulltext_ID_Failure">
                    <xsl:element name="field">
                        <xsl:attribute name="name">fulltext_id_failure</xsl:attribute>
                        <xsl:value-of select="."/>
                    </xsl:element>
                </xsl:for-each>
                
                <!--14 referee -->
                <xsl:for-each select="/Opus/Rdr_Dataset/PersonReferee">
                    <xsl:element name="field">
                        <xsl:attribute name="name">referee</xsl:attribute>
                        <xsl:value-of select="@FirstName" />
                        <xsl:text></xsl:text>
                        <xsl:value-of select="@LastName" />
                    </xsl:element>
                </xsl:for-each>
                
                <!--15 other persons (non-authors) -->
                <xsl:for-each select="/Opus/Rdr_Dataset/*">
                    <xsl:if test="local-name() != 'Person' and local-name() != 'PersonAuthor' and local-name() != 'PersonSubmitter' and substring(local-name(), 1, 6) = 'Person'">
                        <xsl:element name="field">
                            <xsl:attribute name="name">persons</xsl:attribute>
                            <xsl:value-of select="@FirstName" />
                            <xsl:text></xsl:text>
                            <xsl:value-of select="@LastName" />
                        </xsl:element>
                    </xsl:if>
                </xsl:for-each>
                
                
                
                
                
                
                
                
                
                <!--20 collections: project, app_area, institute, ids -->
                <xsl:for-each select="/Opus/Rdr_Dataset/Collection">
                    <xsl:choose>
                        <xsl:when test="@RoleName = 'projects'">
                            <xsl:element name="field">
                                <xsl:attribute name="name">project</xsl:attribute>
                                <xsl:value-of select="@Number" />
                            </xsl:element>
                            <xsl:element name="field">
                                <xsl:attribute name="name">app_area</xsl:attribute>
                                <xsl:value-of select="substring(@Number, 0, 2)" />
                            </xsl:element>
                        </xsl:when>
                        <xsl:when test="@RoleName = 'institutes'">
                            <xsl:element name="field">
                                <xsl:attribute name="name">institute</xsl:attribute>
                                <xsl:value-of select="@Name" />
                            </xsl:element>
                        </xsl:when>
                    </xsl:choose>
                    
                    <xsl:element name="field">
                        <xsl:attribute name="name">collection_ids</xsl:attribute>
                        <xsl:value-of select="@Id" />
                    </xsl:element>
                </xsl:for-each>
                
                
                
                
                
                
                
                <!--26 series ids and series number per id (modeled as dynamic field) -->
                <xsl:for-each select="/Opus/Rdr_Dataset/Series">
                    <xsl:element name="field">
                        <xsl:attribute name="name">series_ids</xsl:attribute>
                        <xsl:value-of select="@Id"/>
                    </xsl:element>
                    
                    <xsl:element name="field">
                        <xsl:attribute name="name">
                            <xsl:text>series_number_for_id_</xsl:text>
                            <xsl:value-of select="@Id"/>
                        </xsl:attribute>
                        <xsl:value-of select="@Number"/>
                    </xsl:element>
                    
                    <xsl:element name="field">
                        <xsl:attribute name="name">
                            <xsl:text>doc_sort_order_for_seriesid_</xsl:text>
                            <xsl:value-of select="@Id"/>
                        </xsl:attribute>
                        <xsl:value-of select="@DocSortOrder"/>
                    </xsl:element>
                </xsl:for-each>
                
                
                
                
                
                
                
                
                
                <!--31 publisher place (single valued) -->
                <xsl:if test="/Opus/Rdr_Dataset/Coverage">
                    <xsl:element name="field">
                        <xsl:attribute name="name">geo_location</xsl:attribute>
                        <xsl:variable name="geolocation" select="concat(
                                'SOUTH-BOUND LATITUDE: ', /Opus/Rdr_Dataset/Coverage/@XMin,
                                ' * WEST-BOUND LONGITUDE: ', /Opus/Rdr_Dataset/Coverage/@YMin,
                                ' * NORTH-BOUND LATITUDE: ', /Opus/Rdr_Dataset/Coverage/@XMax,
                                ' * EAST-BOUND LONGITUDE: ', /Opus/Rdr_Dataset/Coverage/@YMax
                            )" />
                        <!-- <xsl:variable name="geolocation" select="concat('test', /Opus/Rdr_Dataset/Coverage/@XMin)" /> -->
                        <xsl:value-of select="$geolocation" />
                        <xsl:text>&#xA;</xsl:text>
                        <xsl:if test="@ElevationMin != '' and @ElevationMax != ''">
                            <xsl:value-of select="concat(' * ELEVATION MIN: ', @ElevationMin, ' * ELEVATION MAX: ', @ElevationMax)" />
                        </xsl:if>
                        <xsl:if test="@ElevationAbsolut != ''">
                            <xsl:value-of select="concat(' * ELEVATION ABSOLUT: ', @ElevationAbsolut)" />
                        </xsl:if>
                        
                        <xsl:text>&#xA;</xsl:text>
                        <xsl:if test="@DepthMin != '' and @DepthMax != ''">
                            <xsl:value-of select="concat(' * DEPTH MIN: ', @DepthMin, ' * DEPTH MAX: ', @DepthMax)" />
                        </xsl:if>
                        <xsl:if test="@DepthAbsolut != ''">
                            <xsl:value-of select="concat(' * DEPTH ABSOLUT: ', @DepthAbsolut)" />
                        </xsl:if>
                        
                        <xsl:text>&#xA;</xsl:text>
                        <xsl:if test="@TimeMin != '' and @TimeMax != ''">
                            <xsl:value-of select="concat(' * TIME MIN: ', @TimeMin, ' * TIME MAX: ', @TimeMax)" />
                        </xsl:if>
                        <xsl:if test="@TimeAbsolut != ''">
                            <xsl:value-of select="concat(' * TIME ABSOLUT: ', @TimeAbsolut)" />
                        </xsl:if>
                    </xsl:element>
                </xsl:if>
                
                
                
            </xsl:element>
        </xsl:element>
        
    </xsl:template>
    
    
</xsl:stylesheet>