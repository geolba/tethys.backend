import { BaseSchema } from "@adonisjs/lucid/schema";

export default class Roles extends BaseSchema {
    protected tableName = 'roles';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('roles_id_seq')");
            table.string('name', 255).notNullable();
            table.string('display_name', 255);
            table.string('description', 255);
            table.timestamp('created_at', { useTz: false }).nullable();
            table.timestamp('updated_at', { useTz: false }).nullable();
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// CREATE TABLE IF NOT EXISTS roles
// (
//     id integer NOT NULL DEFAULT nextval('roles_id_seq'::regclass),
//     name character varying(255) NOT NULL,
//     display_name character varying(255),
//     description character varying(255),
//     created_at timestamp(0) without time zone,
//     updated_at timestamp(0) without time zone,
//     CONSTRAINT roles_pkey PRIMARY KEY (id)
// )
