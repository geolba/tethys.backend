import { BaseSchema } from "@adonisjs/lucid/schema";

export default class RoleHasPermissions extends BaseSchema {
    // protected tableName = Config.get('rolePermission.role_permission_table', 'role_permissions')
    protected tableName = 'role_has_permissions';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            // table.integer('permission_id').notNullable();
            // table.integer('role_id').notNullable();
            table.integer('permission_id').unsigned().index().notNullable();
            table
                .foreign('permission_id', 'role_has_permissions_permission_id_foreign')
                .references('id')
                .inTable('permissions')
                .onDelete('CASCADE') // delete this when permission is deleted
                .onUpdate('CASCADE');
            table.integer('role_id').unsigned().index().notNullable();
            table
                .foreign('role_id', 'role_has_permissions_role_id_foreign')
                .references('id')
                .inTable('roles')
                .onDelete('CASCADE') // delete this when role is deleted
                .onUpdate('CASCADE');
            table.primary(['permission_id', 'role_id']);
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// CREATE TABLE IF NOT EXISTS role_has_permissions
// (
//     permission_id integer NOT NULL,
//     role_id integer NOT NULL,
//     CONSTRAINT role_has_permissions_pkey PRIMARY KEY (permission_id, role_id),
//     CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id)
//         REFERENCES permissions (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE,
//     CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id)
//         REFERENCES roles (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE
// )
