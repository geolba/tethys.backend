import { BaseSchema } from "@adonisjs/lucid/schema";

export default class LinkDatasetSubjects extends BaseSchema {
    protected tableName = 'link_dataset_subjects';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.integer('subject_id').index('link_dataset_subjects_subject_id_index').notNullable();
            table
                .foreign('subject_id', 'link_dataset_subjects_subject_id_foreign')
                .references('id')
                .inTable('dataset_subjects')
                .onDelete('NO ACTION') // don't delete this when subject is deleted
                .onUpdate('NO ACTION');

            table.integer('document_id').index('link_dataset_subjects_document_id_index').notNullable();
            table
                .foreign('document_id', 'link_dataset_subjects_document_id_foreign')
                .references('id')
                .inTable('documents')
                .onDelete('CASCADE') // delete this when document is deleted
                .onUpdate('CASCADE');

            table.primary(['subject_id', 'document_id']);
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}
// -- Table: link_dataset_subjects
// CREATE TABLE IF NOT EXISTS link_dataset_subjects
// (
//     subject_id integer NOT NULL,
//     document_id integer NOT NULL,
//     CONSTRAINT link_dataset_subjects_pkey PRIMARY KEY (subject_id, document_id),
//     CONSTRAINT link_dataset_subjects_document_id_foreign FOREIGN KEY (document_id)
//         REFERENCES documents (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE,
//     CONSTRAINT link_dataset_subjects_subject_id_foreign FOREIGN KEY (subject_id)
//         REFERENCES dataset_subjects (id) MATCH SIMPLE
//         ON UPDATE NO ACTION
//         ON DELETE NO ACTION
// )

// -- Index: link_dataset_subjects_document_id_index
// -- DROP INDEX IF EXISTS link_dataset_subjects_document_id_index;
// CREATE INDEX IF NOT EXISTS link_dataset_subjects_document_id_index
//     ON link_dataset_subjects USING btree
//     (document_id ASC);
// -- Index: link_dataset_subjects_subject_id_index
// -- DROP INDEX IF EXISTS link_dataset_subjects_subject_id_index;
// CREATE INDEX IF NOT EXISTS link_dataset_subjects_subject_id_index
//     ON link_dataset_subjects USING btree
//     (subject_id ASC);
