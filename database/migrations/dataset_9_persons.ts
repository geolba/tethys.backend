import { PersonNameTypes } from '#contracts/enums';
import { BaseSchema } from "@adonisjs/lucid/schema";

export default class Persons extends BaseSchema {
    protected tableName = 'persons';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('persons_id_seq')");
            table.string('academic_title', 255);
            table.string('date_of_birth', 100);
            table.string('email', 100).notNullable();
            table.string('first_name', 255);
            table.string('last_name', 255);
            table.string('place_of_birth', 255);
            table.string('identifier_orcid', 50);
            table.string('identifier_gnd', 50);
            table.string('identifier_misc', 50);
            table.boolean('status').defaultTo(true);
            table.integer('registered_at');
            // table.string('name_type', 255);
            table.enum('name_type', Object.values(PersonNameTypes)).notNullable();
            table.timestamp('created_at', { useTz: false }).nullable();
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: persons
// CREATE TABLE IF NOT EXISTS persons
// (
//     id integer NOT NULL DEFAULT nextval('persons_id_seq'::regclass),
//     academic_title character varying(255),
//     date_of_birth character varying(100),
//     email character varying(100) NOT NULL,
//     first_name character varying(255),
//     last_name character varying(255),
//     place_of_birth character varying(255),
//     identifier_orcid character varying(50),
//     identifier_gnd character varying(50),
//     identifier_misc character varying(50),
//     status boolean DEFAULT true,
//     registered_at integer,
//     name_type character varying(255),
//     CONSTRAINT persons_pkey PRIMARY KEY (id),
//     CONSTRAINT persons_name_type_check CHECK (name_type::text = ANY (ARRAY['Organizational'::character varying::text, 'Personal'::character varying::text]))
// )
