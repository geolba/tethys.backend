import { PersonRoles, ContributorTypes } from '#contracts/enums';
import { BaseSchema } from "@adonisjs/lucid/schema";

export default class LinkDocumentsPersons extends BaseSchema {
    protected tableName = 'link_documents_persons';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.integer('person_id').index('ix_fk_link_documents_persons_persons').notNullable();
            table
                .foreign('person_id', 'fk_link_documents_persons_persons')
                .references('id')
                .inTable('persons')
                .onDelete('NO ACTION') // don't delete this when license is deleted
                .onUpdate('NO ACTION');
            table.integer('document_id').index('ix_fk_link_persons_documents_documents').notNullable();
            table
                .foreign('document_id', 'fk_link_persons_documents_documents')
                .references('id')
                .inTable('documents')
                .onDelete('CASCADE') // delete this when document is deleted
                .onUpdate('CASCADE');

            // table.string('role').notNullable().defaultTo("'other'");
            table.enum('role', Object.values(PersonRoles)).notNullable().defaultTo('other');

            table.specificType('sort_order', 'smallint').notNullable();
            table.boolean('allow_email_contact').notNullable().defaultTo(false);

            // table.string('contributor_type');
            table.enum('contributor_type', Object.values(ContributorTypes));

            table.primary(['person_id', 'document_id', 'role']);
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: gba.link_documents_persons
// -- DROP TABLE IF EXISTS gba.link_documents_persons;

// CREATE TABLE IF NOT EXISTS link_documents_persons
// (
//     person_id integer NOT NULL,
//     document_id integer NOT NULL,
//     role character varying(255) NOT NULL DEFAULT 'other'::character varying,
//     sort_order smallint NOT NULL,
//     allow_email_contact boolean NOT NULL DEFAULT false,
//     contributor_type character varying(255),
//     CONSTRAINT link_documents_persons_pkey PRIMARY KEY (person_id, document_id, role),
//     CONSTRAINT fk_link_documents_persons_persons FOREIGN KEY (person_id)
//         REFERENCES persons (id) MATCH SIMPLE
//         ON UPDATE NO ACTION
//         ON DELETE NO ACTION,
//     CONSTRAINT fk_link_persons_documents_documents FOREIGN KEY (document_id)
//         REFERENCES documents (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE,
//     CONSTRAINT link_documents_persons_contributor_type_check CHECK (contributor_type::text = ANY (ARRAY['ContactPerson'::character varying::text, 'DataCollector'::character varying::text, 'DataCurator'::character varying::text, 'DataManager'::character varying::text, 'Distributor'::character varying::text, 'Editor'::character varying::text, 'HostingInstitution'::character varying::text, 'Producer'::character varying::text, 'ProjectLeader'::character varying::text, 'ProjectManager'::character varying::text, 'ProjectMember'::character varying::text, 'RegistrationAgency'::character varying::text, 'RegistrationAuthority'::character varying::text, 'RelatedPerson'::character varying::text, 'Researcher'::character varying::text, 'ResearchGroup'::character varying::text, 'RightsHolder'::character varying::text, 'Sponsor'::character varying::text, 'Supervisor'::character varying::text, 'WorkPackageLeader'::character varying::text, 'Other'::character varying::text])),
//     CONSTRAINT link_documents_persons_role_check CHECK (role::text = ANY (ARRAY['author'::character varying::text, 'contributor'::character varying::text, 'other'::character varying::text]))
// )

// -- Index: ix_fk_link_documents_persons_persons
// -- DROP INDEX IF EXISTS ix_fk_link_documents_persons_persons;
// CREATE INDEX IF NOT EXISTS ix_fk_link_documents_persons_persons
//     ON link_documents_persons USING btree
//     (person_id ASC);
// -- Index: ix_fk_link_persons_documents_documents
// -- DROP INDEX IF EXISTS ix_fk_link_persons_documents_documents;
// CREATE INDEX IF NOT EXISTS ix_fk_link_persons_documents_documents
//     ON link_documents_persons USING btree
//     (document_id ASC);
