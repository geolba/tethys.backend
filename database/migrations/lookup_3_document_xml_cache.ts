import { BaseSchema } from "@adonisjs/lucid/schema";

export default class DocumentXmlCache extends BaseSchema {
    protected tableName = 'document_xml_cache';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('document_id').primary();
            table.integer('xml_version').notNullable();
            table.string('server_date_modified', 50);
            table.text('xml_data');
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: document_xml_cache
// CREATE TABLE IF NOT EXISTS document_xml_cache
// (
//     document_id integer NOT NULL,
//     xml_version integer NOT NULL,
//     server_date_modified character varying(50),
//     xml_data text,
//     CONSTRAINT document_xml_cache_pkey PRIMARY KEY (document_id)
// )
