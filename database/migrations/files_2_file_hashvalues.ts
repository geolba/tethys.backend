import { BaseSchema } from "@adonisjs/lucid/schema";

export default class FileHashvalues extends BaseSchema {
    protected tableName = 'file_hashvalues';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.integer('file_id').notNullable();
            table
                .foreign('file_id', 'file_hashvalues_file_id_foreign')
                .references('id')
                .inTable('document_files')
                .onDelete('CASCADE') // delete this when document_file is deleted
                .onUpdate('CASCADE');
            table.string('type', 50).notNullable();
            table.string('value').notNullable();
            table.primary(['file_id', 'type']);
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: file_hashvalues
// CREATE TABLE IF NOT EXISTS file_hashvalues
// (
//     file_id integer NOT NULL,
//     type character varying(50) NOT NULL,
//     value character varying(255) NOT NULL,
//     CONSTRAINT file_hashvalues_pkey PRIMARY KEY (file_id, type),
//     CONSTRAINT file_hashvalues_file_id_foreign FOREIGN KEY (file_id)
//         REFERENCES document_files (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE
// )
