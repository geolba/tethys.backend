import { BaseSchema } from "@adonisjs/lucid/schema";

export default class CollectionsRoles extends BaseSchema {
    protected tableName = 'collections_roles';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('collections_roles_id_seq')");
            table.string('name', 255).notNullable();
            table.string('oai_name', 255).notNullable();
            table.integer('position').notNullable();
            table.boolean('visible').notNullable().defaultTo(true);
            table.boolean('visible_frontdoor').notNullable().defaultTo(true);
            table.boolean('visible_oai').notNullable().defaultTo(true);
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: collection roles
// CREATE TABLE IF NOT EXISTS collections_roles
// (
//     id integer NOT NULL DEFAULT nextval('collections_roles_id_seq'::regclass),
//     name character varying(255) NOT NULL,
//     oai_name character varying(255) NOT NULL,
//     "position" integer NOT NULL,
//     visible boolean NOT NULL DEFAULT true,
//     visible_frontdoor boolean NOT NULL DEFAULT true,
//     visible_oai boolean NOT NULL DEFAULT true,
//     CONSTRAINT collections_roles_pkey PRIMARY KEY (id)
// )
