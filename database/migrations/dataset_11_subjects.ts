import { SubjectTypes } from '#contracts/enums';
import { BaseSchema } from "@adonisjs/lucid/schema";

export default class DatasetSubjects extends BaseSchema {
    protected tableName = 'dataset_subjects';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.bigIncrements('id').defaultTo("nextval('dataset_subjects_id_seq')");
            table.string('language', 3);
            // table.string('type', 255).notNullable().defaultTo('uncontrolled');
            table.enum('type', Object.keys(SubjectTypes)).defaultTo('uncontrolled');
            table.string('value', 255).notNullable();
            table.string('external_key', 255);
            table.timestamp('created_at', { useTz: false }).nullable();
            table.timestamp('updated_at', { useTz: false }).nullable();
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: dataset_subjects
// CREATE TABLE IF NOT EXISTS dataset_subjects
// (
//     id bigint NOT NULL DEFAULT nextval('dataset_subjects_id_seq'::regclass),
//     language character varying(3),
//     type character varying(255) NOT NULL,
//     value character varying(255) NOT NULL,
//     external_key character varying(255),
//     created_at timestamp(0) without time zone,
//     updated_at timestamp(0) without time zone,
//     CONSTRAINT dataset_subjects_pkey PRIMARY KEY (id),
//     CONSTRAINT dataset_subjects_type_check CHECK (type::text = 'uncontrolled'::text)
// )
