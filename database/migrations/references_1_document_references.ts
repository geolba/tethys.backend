import { RelationTypes, ReferenceIdentifierTypes } from '#contracts/enums';
import { BaseSchema } from "@adonisjs/lucid/schema";

export default class DocumentReferences extends BaseSchema {
    protected tableName = 'document_references';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('document_references_id_seq')");
            table.integer('document_id').unsigned().notNullable();
            table
                .foreign('document_id', 'document_references_document_id_foreign')
                .references('id')
                .inTable('documents')
                .onDelete('CASCADE') // delete this reference when document is deleted
                .onUpdate('CASCADE');
            // table.string('type').notNullable();
            table.enum('type', Object.keys(ReferenceIdentifierTypes)).notNullable();
            // table.string('relation').notNullable();
            table.enum('relation', Object.keys(RelationTypes)).notNullable();
            table.string('value').notNullable();
            table.string('label').notNullable();
            table.timestamp('created_at', { useTz: false }).nullable();
            table.timestamp('updated_at', { useTz: false }).nullable();
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: document_references
// CREATE TABLE IF NOT EXISTS document_references
// (
//     id integer NOT NULL DEFAULT nextval('document_references_id_seq'::regclass),
//     document_id integer NOT NULL,
//     type character varying(255) NOT NULL,
//     relation character varying(255) NOT NULL,
//     value character varying(255) NOT NULL,
//     label character varying(255) NOT NULL,
//     created_at timestamp(0) without time zone,
//     updated_at timestamp(0) without time zone,
//     CONSTRAINT document_references_pkey PRIMARY KEY (id),
//     CONSTRAINT document_references_document_id_foreign FOREIGN KEY (document_id)
//         REFERENCES documents (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE,
//     CONSTRAINT document_references_relation_check CHECK (relation::text = ANY (ARRAY['IsSupplementTo'::character varying::text, 'IsSupplementedBy'::character varying::text, 'IsContinuedBy'::character varying::text, 'Continues'::character varying::text, 'IsNewVersionOf'::character varying::text, 'IsPartOf'::character varying::text, 'HasPart'::character varying::text, 'Compiles'::character varying::text, 'IsVariantFormOf'::character varying::text])),
//     CONSTRAINT document_references_type_check CHECK (type::text = ANY (ARRAY['DOI'::character varying::text, 'Handle'::character varying::text, 'ISBN'::character varying::text, 'ISSN'::character varying::text, 'URL'::character varying::text, 'URN'::character varying::text]))
// )
