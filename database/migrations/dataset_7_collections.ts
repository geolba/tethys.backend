import { BaseSchema } from "@adonisjs/lucid/schema";

export default class Collections extends BaseSchema {
    protected tableName = 'collections';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').defaultTo("nextval('collections_id_seq')");
            table.integer('role_id').unsigned();
            table
                .foreign('role_id', 'collections_role_id_foreign')
                .references('id')
                .inTable('collections_roles')
                .onDelete('CASCADE') // delete this collection when collection_role is deleted
                .onUpdate('CASCADE');
            table.string('number', 255);
            table.string('name', 255).notNullable();
            table.string('oai_subset', 255);
            table.integer('parent_id').unsigned();
            table
                .foreign('parent_id', 'collections_parent_id_foreign')
                .references('id')
                .inTable('collections')
                .onDelete('CASCADE') // delete this collection when parent collection is deleted
                .onUpdate('CASCADE');
            table.boolean('visible').notNullable().defaultTo(true);
            table.boolean('visible_publish').notNullable().defaultTo(true);
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: collections
// CREATE TABLE IF NOT EXISTS collections
// (
//     id integer NOT NULL DEFAULT nextval('collections_id_seq'::regclass),
//     role_id integer,
//     "number" character varying(255),
//     name character varying(255) NOT NULL,
//     oai_subset character varying(255),
//     parent_id integer,
//     visible boolean NOT NULL DEFAULT true,
//     visible_publish boolean NOT NULL DEFAULT true,
//     CONSTRAINT collections_pkey PRIMARY KEY (id),
//     CONSTRAINT collections_parent_id_foreign FOREIGN KEY (parent_id)
//         REFERENCES collections (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE,
// 	CONSTRAINT collections_role_id_foreign FOREIGN KEY (role_id)
//         REFERENCES collections_roles (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE
// )
