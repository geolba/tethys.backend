import { BaseSchema } from "@adonisjs/lucid/schema";

export default class LinkAccountsRoles extends BaseSchema {
    // protected tableName = Config.get('rolePermission.user_role_table', 'user_roles')
    protected tableName = 'link_accounts_roles';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.integer('account_id').index().unsigned().notNullable();
            table
                .foreign('account_id', 'link_accounts_roles_account_id_foreign')
                .references('id')
                .inTable('accounts')
                // .inTable(Config.get('rolePermission.user_table', 'users'))
                .onDelete('CASCADE') // delete this when account is delete
                .onUpdate('CASCADE');
            table.integer('role_id').index().unsigned().notNullable();
            table
                .foreign('role_id', 'link_accounts_roles_role_id_foreign')
                .references('id')
                .inTable('roles')
                // .inTable(Config.get('rolePermission.role_table', 'roles'))
                .onDelete('CASCADE') // delete this when account is delete
                .onUpdate('CASCADE');
            table.primary(['account_id', 'role_id']);
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// CREATE TABLE IF NOT EXISTS link_accounts_roles
// (
//     account_id integer NOT NULL,
//     role_id integer NOT NULL,
//     CONSTRAINT link_accounts_roles_pkey PRIMARY KEY (account_id, role_id),
//     CONSTRAINT link_accounts_roles_account_id_foreign FOREIGN KEY (account_id)
//         REFERENCES accounts (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE,
//     CONSTRAINT link_accounts_roles_role_id_foreign FOREIGN KEY (role_id)
//         REFERENCES roles (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE
// )
