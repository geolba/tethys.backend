import { BaseSchema } from "@adonisjs/lucid/schema";

export default class Accounts extends BaseSchema {
    protected tableName = 'accounts';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('accounts_id_seq')");
            table.string('login', 20).notNullable();
            table.string('password', 60).notNullable();
            table.string('email', 255).unique().notNullable();
            table.string('first_name', 255).nullable();
            table.string('last_name', 255).nullable();
            table.string('remember_token');
            table.timestamp('created_at');
            table.timestamp('updated_at');
            table.text("two_factor_secret").nullable();
            table.text("two_factor_recovery_codes").nullable();
            table.smallint('state').nullable();
            table.bigint('last_counter').nullable();
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// CREATE TABLE IF NOT EXISTS accounts
// (
//     id integer NOT NULL DEFAULT nextval('accounts_id_seq'::regclass),
//     login character varying(20) NOT NULL,
//     password character varying(60) NOT NULL,
//     email character varying(255)  NOT NULL,
//     first_name character varying(255),
//     last_name character varying(255),
//     remember_token character varying(100),
//     created_at timestamp(0) without time zone,
//     updated_at timestamp(0) without time zone,
//     CONSTRAINT accounts_pkey PRIMARY KEY (id),
//     CONSTRAINT accounts_email_unique UNIQUE (email)
//     two_factor_secret text COLLATE pg_catalog."default",
//     two_factor_recovery_codes text COLLATE pg_catalog."default",
//     state smallint,
//     last_counter  bigint,
// )

// ALTER TABLE gba.accounts
// ADD COLUMN two_factor_secret text COLLATE pg_catalog."default",
// ADD COLUMN two_factor_recovery_codes text COLLATE pg_catalog."default",
// ADD COLUMN state smallint,
// ADD COLUMN last_counter bigint;




// CREATE TABLE IF NOT EXISTS gba.totp_secrets
// (
//     id integer NOT NULL,
// 	user_id integer NOT NULL,
//     state smallint,
//     last_counter  bigint,
//     two_factor_secret text COLLATE pg_catalog."default",
//     two_factor_recovery_codes text COLLATE pg_catalog."default",
//     created_at timestamp(0) without time zone,
//     updated_at timestamp(0) without time zone,
// 	CONSTRAINT totp_secrets_pkey PRIMARY KEY (id),
//     CONSTRAINT totp_secrets_user_id_foreign FOREIGN KEY (user_id)
//         REFERENCES gba.accounts (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE
// );

// CREATE SEQUENCE IF NOT EXISTS gba.totp_secrets_id_seq
//     INCREMENT 1
//     START 1
//     MINVALUE 1
//     MAXVALUE 2147483647
//     CACHE 1
//     OWNED BY gba.totp_secrets.id;

// ALTER SEQUENCE gba.totp_secrets_id_seq
//     OWNER TO tethys_admin;

// GRANT ALL ON SEQUENCE gba.totp_secrets_id_seq TO tethys_admin;

// ALTER TABLE gba.totp_secrets ALTER COLUMN id SET DEFAULT nextval('gba.totp_secrets_id_seq');
