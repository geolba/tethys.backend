import { TitleTypes } from '#contracts/enums';
import { BaseSchema } from "@adonisjs/lucid/schema";

export default class DatasetTitles extends BaseSchema {
    protected tableName = 'dataset_titles';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('dataset_titles_id_seq')");
            table.integer('document_id').unsigned().notNullable();
            table
                .foreign('document_id', 'dataset_titles_document_id_foreign')
                .references('id')
                .inTable('documents')
                .onDelete('CASCADE') // delete this title when document is deleted
                .onUpdate('CASCADE');
            // table.string('type', 255).notNullable();
            table.enum('type', Object.values(TitleTypes)).notNullable();
            table.string('value', 255).notNullable();
            table.string('language', 3).notNullable();
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: dataset_titles
// CREATE TABLE IF NOT EXISTS dataset_titles
// (
//     id integer NOT NULL DEFAULT nextval('dataset_titles_id_seq'::regclass),
//     document_id integer NOT NULL,
//     type character varying(255) NOT NULL,
//     value character varying(255) NOT NULL,
//     language character varying(3) NOT NULL,
//     CONSTRAINT dataset_titles_pkey PRIMARY KEY (id),
//     CONSTRAINT dataset_titles_document_id_foreign FOREIGN KEY (document_id)
//         REFERENCES documents (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE,
//     CONSTRAINT dataset_titles_type_check CHECK (type::text = ANY (ARRAY['Main'::character varying::text, 'Sub'::character varying::text, 'Alternative'::character varying::text, 'Translated'::character varying::text, 'Other'::character varying::text]))
// )
