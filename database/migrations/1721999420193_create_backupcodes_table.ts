// node ace make:migration backupcodes
import { BaseSchema } from '@adonisjs/lucid/schema';

export default class extends BaseSchema {
    protected tableName = 'backupcodes';

    async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id');

            table.integer('user_id').unsigned();
            table.string('code', 64).notNullable();
            table.boolean('used').defaultTo(false);

            table.foreign('user_id').references('id').inTable('accounts').onUpdate('CASCADE').onDelete('CASCADE');

            table.index('user_id', 'backupcodes_uid');
        });
    }

    async down() {
        this.schema.dropTable(this.tableName);
    }
}

// CREATE SEQUENCE IF NOT EXISTS gba.backupcodes_id_seq
//     INCREMENT 1
//     START 1
//     MINVALUE 1
//     MAXVALUE 2147483647
//     CACHE 1;

// ALTER SEQUENCE gba.backupcodes_id_seq
//     OWNER TO tethys_admin;

// GRANT ALL ON SEQUENCE gba.backupcodes_id_seq TO tethys_admin;

// GRANT USAGE ON SEQUENCE gba.backupcodes_id_seq TO tethys_app;

// CREATE TABLE IF NOT EXISTS gba.backupcodes
// (
//     id integer NOT NULL DEFAULT nextval('gba.backupcodes_id_seq'::regclass),
//     user_id integer,
//     code character varying(64) NOT NULL,
// 	used boolean NOT NULL DEFAULT false,
//     CONSTRAINT backupcodes_user_id_foreign FOREIGN KEY (user_id)
//         REFERENCES gba.accounts (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE,
//     CONSTRAINT backupcodes_pkey PRIMARY KEY (id)
// )

// CREATE INDEX IF NOT EXISTS backupcodes_uid
//     ON gba.backupcodes USING btree
//     (user_id ASC);
