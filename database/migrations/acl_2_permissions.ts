import { BaseSchema } from "@adonisjs/lucid/schema";

export default class Permissions extends BaseSchema {
    protected tableName = 'permissions';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('permissions_id_seq')");
            table.string('name', 255).unique().notNullable();
            table.string('display_name', 100).notNullable();
            table.string('description', 255);
            // table.timestamp('created_at');
            // table.timestamp('updated_at');
            table.timestamp('created_at', { useTz: false });
            table.timestamp('updated_at', { useTz: false });
        });
    }

    public async down() {
        this.schema.dropTableIfExists(this.tableName);
    }
}

// CREATE TABLE IF NOT EXISTS permissions
// (
//     id integer NOT NULL DEFAULT nextval('permissions_id_seq'::regclass),
//     name character varying(255) NOT NULL,
//     display_name character varying(100) NOT NULL,
//     description character varying(255),
//     created_at timestamp(0) without time zone,
//     updated_at timestamp(0) without time zone,
//     CONSTRAINT permissions_pkey PRIMARY KEY (id),
//     CONSTRAINT permissions_name_unique UNIQUE (name)
// )
