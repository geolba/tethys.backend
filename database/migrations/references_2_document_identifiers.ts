import { IdentifierTypes } from '#contracts/enums';
import { BaseSchema } from "@adonisjs/lucid/schema";

export default class DocumentIdentifiers extends BaseSchema {
    protected tableName = 'document_identifiers';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('document_identifiers_id_seq')");
            table.integer('document_id').unsigned().notNullable();
            table
                .foreign('document_id', 'document_identifiers_document_id_foreign')
                .references('id')
                .inTable('documents')
                .onDelete('CASCADE') // delete this identifier when document is deleted
                .onUpdate('CASCADE');
            // table.string('type').notNullable();
            table.enum('type', Object.keys(IdentifierTypes)).notNullable();
            table.string('value').notNullable();
            table.timestamp('created_at', { useTz: false }).nullable();
            table.timestamp('updated_at', { useTz: false }).nullable();
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: document_identifiers
// CREATE TABLE IF NOT EXISTS document_identifiers
// (
//     id integer NOT NULL DEFAULT nextval('document_identifiers_id_seq'::regclass),
//     document_id integer NOT NULL,
//     type character varying(255) NOT NULL,
//     value character varying(255) NOT NULL,
//     created_at timestamp(0) without time zone,
//     updated_at timestamp(0) without time zone,
//     CONSTRAINT document_identifiers_pkey PRIMARY KEY (id),
//     CONSTRAINT document_identifiers_document_id_foreign FOREIGN KEY (document_id)
//         REFERENCES documents (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE,
//     CONSTRAINT document_identifiers_type_check CHECK (type::text = ANY (ARRAY['doi'::character varying::text, 'handle'::character varying::text, 'isbn'::character varying::text, 'issn'::character varying::text, 'url'::character varying::text, 'urn'::character varying::text]))
// )
