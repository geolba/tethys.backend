import { BaseSchema } from "@adonisjs/lucid/schema";

export default class Projects extends BaseSchema {
    protected tableName = 'projects';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('projects_id_seq')");
            table.string('label', 50).notNullable();
            table.string('name', 255).notNullable();
            table.string('description', 255).nullable();
            table.timestamp('created_at', { useTz: false }).nullable();
            table.timestamp('updated_at', { useTz: false }).nullable();
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: project
// CREATE TABLE IF NOT EXISTS projects
// (
//     id integer NOT NULL DEFAULT nextval('projects_id_seq'::regclass),
//     label character varying(50) NOT NULL,
//     name character varying(255) NOT NULL,
//     description character varying(2500),
//     created_at timestamp(0) without time zone,
//     updated_at timestamp(0) without time zone,
//     CONSTRAINT projects_pkey PRIMARY KEY (id)
// )
