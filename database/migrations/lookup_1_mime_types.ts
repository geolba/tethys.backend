import { BaseSchema } from "@adonisjs/lucid/schema";

export default class MimeTypes extends BaseSchema {
    protected tableName = 'mime_types';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id').primary().defaultTo("nextval('mime_types_id_seq')");
            table.string('name', 255).notNullable();
            table.string('file_extension', 255).notNullable();
            table.boolean('enabled').notNullable().defaultTo(false);
            table.timestamp('created_at', { useTz: false }).nullable();
            table.timestamp('updated_at', { useTz: false }).nullable();
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: mime_types
// CREATE TABLE IF NOT EXISTS mime_types
// (
//     id integer NOT NULL DEFAULT nextval('mime_types_id_seq'::regclass),
//     name character varying(255) NOT NULL,
//     file_extension character varying(255) NOT NULL,
//     enabled boolean NOT NULL DEFAULT false,
//     created_at timestamp(0) without time zone,
//     updated_at timestamp(0) without time zone,
//     CONSTRAINT mime_types_pkey PRIMARY KEY (id)
// )
