// node ace make:migration appconfig
import { BaseSchema } from '@adonisjs/lucid/schema';

export default class extends BaseSchema {
    protected tableName = 'appconfigs';

    async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.string('appid', 32).notNullable();
            table.string('configkey', 64).notNullable();
            table.text('configvalue');
            table.bigInteger('type');
            table.smallint('lazy');
            table.unique(['appid', 'configkey']) // Unique constraint on the combination
        });
    }

    async down() {
        this.schema.dropTable(this.tableName);
    }
}
