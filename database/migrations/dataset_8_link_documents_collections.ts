import { BaseSchema } from "@adonisjs/lucid/schema";

export default class LinkDocumentsCollections extends BaseSchema {
    protected tableName = 'link_documents_collections';

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.integer('collection_id').index('link_documents_collections_collection_id_index').notNullable();
            table
                .foreign('collection_id', 'link_documents_collections_collection_id_foreign')
                .references('id')
                .inTable('collections')
                .onDelete('CASCADE') // don't delete this when collection is deleted
                .onUpdate('CASCADE');
            table.integer('document_id').index('link_documents_collections_document_id_index').notNullable();
            table
                .foreign('document_id', 'link_documents_collections_document_id_foreign')
                .references('id')
                .inTable('documents')
                .onDelete('CASCADE') // don't delete this when document is deleted
                .onUpdate('CASCADE');
            table.primary(['collection_id', 'document_id']);
        });
    }

    public async down() {
        this.schema.dropTable(this.tableName);
    }
}

// -- Table: link_documents_collections
// CREATE TABLE IF NOT EXISTS link_documents_collections
// (
//     collection_id integer NOT NULL,
//     document_id integer NOT NULL,
//     CONSTRAINT link_documents_collections_pkey PRIMARY KEY (collection_id, document_id),
//     CONSTRAINT link_documents_collections_collection_id_foreign FOREIGN KEY (collection_id)
//         REFERENCES collections (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE,
//     CONSTRAINT link_documents_collections_document_id_foreign FOREIGN KEY (document_id)
//         REFERENCES documents (id) MATCH SIMPLE
//         ON UPDATE CASCADE
//         ON DELETE CASCADE
// )

// -- Index: link_documents_collections_collection_id_index
// -- DROP INDEX IF EXISTS link_documents_collections_collection_id_index;
// CREATE INDEX IF NOT EXISTS link_documents_collections_collection_id_index
//     ON link_documents_collections USING btree (collection_id ASC);
// -- Index: link_documents_collections_document_id_index
// -- DROP INDEX IF EXISTS link_documents_collections_document_id_index;
// CREATE INDEX IF NOT EXISTS link_documents_collections_document_id_index
//     ON link_documents_collections USING btree (document_id ASC);
