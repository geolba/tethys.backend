################## First Stage - Creating base #########################

# Created a variable to hold our node base image
ARG NODE_IMAGE=node:20-bookworm-slim

FROM $NODE_IMAGE AS base
# Install dumb-init and ClamAV, and perform ClamAV database update
RUN apt update \
    && apt-get install -y dumb-init clamav clamav-daemon nano \ 
    && rm -rf /var/lib/apt/lists/* \
    # Creating folders and changing ownerships
    && mkdir -p /home/node/app && chown node:node /home/node/app \
    &&  mkdir -p /var/lib/clamav \
    && mkdir /usr/local/share/clamav \
    && chown -R node:clamav /var/lib/clamav /usr/local/share/clamav /etc/clamav \
    # permissions
    && mkdir /var/run/clamav \
    && chown node:clamav /var/run/clamav \
    && chmod 750 /var/run/clamav
# -----------------------------------------------
# --- ClamAV & FeshClam -------------------------
# -----------------------------------------------
# RUN \
#   chmod 644 /etc/clamav/freshclam.conf && \
#   freshclam && \
#   mkdir /var/run/clamav && \
  # chown -R clamav:root /var/run/clamav

# # initial update of av databases
# RUN freshclam

# Configure Clam AV...
COPY --chown=node:clamav ./*.conf /etc/clamav/

# # permissions
# RUN mkdir /var/run/clamav && \
#     chown node:clamav /var/run/clamav && \
#     chmod 750 /var/run/clamav
# Setting the working directory
WORKDIR /home/node/app
# Changing the current active user to "node"
USER node

# initial update of av databases
RUN freshclam

# VOLUME /var/lib/clamav
COPY --chown=node:clamav docker-entrypoint.sh /home/node/app/docker-entrypoint.sh
RUN chmod +x /home/node/app/docker-entrypoint.sh
ENV TZ="Europe/Vienna"




################## Second Stage - Installing dependencies ##########
# In this stage, we will start installing dependencies
FROM base AS dependencies
# We copy all package.* files to the working directory
COPY --chown=node:node ./package*.json ./
# We run NPM CI to install the exact versions of dependencies
RUN npm ci
# Lastly, we copy all the files with active user
COPY --chown=node:node . .


################## Third Stage - Building Stage #####################
# In this stage, we will start building dependencies
FROM dependencies AS build
ENV NODE_ENV=production
# We run "node ace build" to build the app (dist folder) for production
RUN node ace build --ignore-ts-errors
# RUN node ace build --production
# RUN node ace build --ignore-ts-errors


################## Final Stage - Production #########################
# In this final stage, we will start running the application
FROM base AS production
# Here, we include all the required environment variables
# ENV NODE_ENV=production
# ENV PORT=$PORT
# ENV HOST=0.0.0.0

# Copy package.* to the working directory with active user
COPY --chown=node:node ./package*.json ./
# We run NPM CI to install the exact versions of production dependencies
RUN npm ci --omit=dev
# Copy files to the working directory from the build folder the user
COPY --chown=node:node --from=build /home/node/app/build .
# Expose port
EXPOSE 3333
ENTRYPOINT ["/home/node/app/docker-entrypoint.sh"]
# Run the command to start the server using "dumb-init"
CMD [ "dumb-init", "node", "bin/server.js" ]