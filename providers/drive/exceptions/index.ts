// const utils_1 = require("@poppinss/utils");
// import * as utils_1 from "@poppinss/utils";
import { Exception } from '@poppinss/utils';

/**
 * Custom exception for when a file cannot be deleted from a specified location
 */
export class CannotDeleteFileException extends Exception {
    location: string;
    original: any;

    static invoke(location: string, original: any): CannotDeleteFileException {
        const error = new this(`Cannot delete file at location "${location}"`, {
            code: 'E_CANNOT_DELETE_FILE',
            status: 500,
        });
        error.location = location;
        error.original = original;
        return error;
    }
}
/**
 * Unable to move file from source to destination
 */
export class CannotMoveFileException extends Exception {
    source: string;
    destination: string;
    original: any;
    static invoke(source: string, destination: string, original: any): CannotMoveFileException {
        const error = new this(`Cannot move file from "${source}" to "${destination}"`, {
            status: 500,
            code: 'E_CANNOT_MOVE_FILE',
        });
        error.source = source;
        error.destination = destination;
        error.original = original;
        return error;
    }
}
/**
 * Custom exception for when file metadata cannot be retrieved
 */
export class CannotGetMetaDataException extends Exception {
    location: string;
    operation: string;
    original: any;

    static invoke(location: string, operation: string, original: any): CannotGetMetaDataException {
        const error = new this(`Unable to retrieve the "${operation}" for file at location "${location}"`, {
            code: 'E_CANNOT_GET_METADATA',
            status: 500,
        });
        error.location = location;
        error.operation = operation;
        error.original = original;
        return error;
    }
}

/**
 * Given location is trying to traverse beyond the root path
 */
export class PathTraversalDetectedException extends Exception {
    location: string;
    static invoke(location: string) {
        const error = new this(`Path traversal detected: "${location}"`, {
            code: 'E_PATH_TRAVERSAL_DETECTED',
            status: 500,
        });
        error.location = location;
        return error;
    }
}

/**
 * Unable to list directory contents of given location
 */
export class CannotListDirectoryException extends Exception {
    location: string;
    original: any;
    static invoke(location: string, original: any): CannotListDirectoryException {
        const error = new this(`Cannot list directory contents of location "${location}"`, {
            status: 500,
            code: 'E_CANNOT_LIST_DIRECTORY',
        });
        error.location = location;
        error.original = original;
        return error;
    }
}

/**
 * Unable to generate url for a file. The assets serving is disabled
 */
export class CannotGenerateUrlException extends Exception {
    location: string;
    static invoke(location: string, diskName: string): CannotGenerateUrlException {
        const error = new this(
            `Cannot generate URL for location "${location}". Make sure to set "serveFiles = true" for "${diskName}" disk`,
            {
                status: 500,
                code: 'E_CANNOT_GENERATE_URL',
            },
        );

        error.location = location;
        return error;
    }
}
