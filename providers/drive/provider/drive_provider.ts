import type { ApplicationService } from '@adonisjs/core/types';
import { RuntimeException } from '@poppinss/utils';
// import DriveManager from '../src/drive_manager.js';
import { DriveConfig } from '../src/types/drive.js';



export default class DriveProvider {
    constructor(protected app: ApplicationService) {}

    /**
     * Register bindings to the container
     */
    async register() {
        const { default: DriveManager } = await import('../src/drive_manager.js');
        this.app.container.singleton(DriveManager, async () => {
            // 1. import the oai configuration
            // const ttl: number = 86400;
            const config: DriveConfig = this.app.config.get('drive');
            // const config: DriveConfig | null = await configProvider.resolve(this.app, driveConfigProvider);
            // const vite = await this.app.container.make("vite");
            if (!config) {
                throw new RuntimeException('Invalid "config/drive.ts" file. Make sure you are using the "defineConfig" method');
            }

            return new DriveManager(this.app, config);
        });
    }

    /**
     * Register drive with the container
     */
    // registerDrive() {
    //     this.app.container.singleton('Adonis/Core/Drive', () => {
    //         const { DriveManager } = require('../src/DriveManager');
    //         const Router = this.app.container.resolveBinding('Adonis/Core/Route');
    //         const Config = this.app.container.resolveBinding('Adonis/Core/Config');
    //         const Logger = this.app.container.resolveBinding('Adonis/Core/Logger');
    //         return new DriveManager(this.app, Router, Logger, Config.get('drive'));
    //     });
    // }

    /**
     * The container bindings have booted
     */
    async boot() {}

    /**
     * The application has been booted
     */
    async start() {}

    /**
     * The process has been started
     */
    async ready() {}

    /**
     * Preparing to shutdown the app
     */
    async shutdown() {}
}
