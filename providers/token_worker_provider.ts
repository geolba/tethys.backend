// import TokenWorkerService from "#app/Library/Oai/TokenWorkerSerice";
import type { ApplicationService } from "@adonisjs/core/types";
import TokenWorkerContract from "#library/Oai/TokenWorkerContract";
// import TokenWorkerService from "#library/Oai/TokenWorkerSerice";

/*
|--------------------------------------------------------------------------
| Provider
|--------------------------------------------------------------------------
|
| Your application is not ready when this file is loaded by the framework.
| Hence, the top level imports relying on the IoC container will not work.
| You must import them inside the life-cycle methods defined inside
| the provider class.
|
| @example:
|
| public async ready () {
|   const Database = this.app.container.resolveBinding('Adonis/Lucid/Database')
|   const Event = this.app.container.resolveBinding('Adonis/Core/Event')
|   Event.on('db:query', Database.prettyPrint)
| }
|
*/

// https://github.com/adonisjs/core/discussions/4268
export default class TokenWorkerProvider {
    public static needsApplication = true;
    private tokenWorkerInstance: TokenWorkerContract | null = null;

    constructor(protected app: ApplicationService) {}

    public async register() {
        await this.setupDependancyInjectionBindings()
      }

    public async setupDependancyInjectionBindings() {
        // const TokenWorkerService = await import('#app/Library/Oai/TokenWorkerSerice');
        const { default: TokenWorkerService } = await import('#library/Oai/TokenWorkerSerice');
        // Register your own bindings
        // Bind TokenWorker to the IoC container
        this.app.container.singleton(TokenWorkerContract, () => {
            // 1. import the oai configuration
            const ttl: number = 86400;

            // 2. import our REDIS wrapper class
            // const TokenWorkerService = require('#library/Oai/TokenWorkerSerice').default;
            this.tokenWorkerInstance = new TokenWorkerService(ttl);

            // 3. return a new instance
            return this.tokenWorkerInstance;
        });

        // this.app.container.singleton(
        //     '#app/Library/Oai/TokenWorkerContract',
        //     () => {
                
        //         // 1. import the oai configuration
        //     const ttl: number = 86400;

        //     // 2. import our REDIS wrapper class
        //     // const TokenWorkerService = require('#app/Library/Oai/TokenWorkerSerice').default;
        //     this.tokenWorkerInstance = new TokenWorkerService(ttl);

        //     // 3. return a new instance
        //     return this.tokenWorkerInstance;
        //     }
        //   )
    }

    // public async boot() {
    //     // All bindings are ready, feel free to use them
    //     // optionally do some initial setup
    // }

    // public async ready() {
    //     // App is ready
    // }

    public async shutdown() {
        // console.log('TokenServerProvider shutdown()');
        // Cleanup, since app is going down
        if (this.tokenWorkerInstance) {
            // Call the disconnect method when the application is shutting down
            await this.tokenWorkerInstance.close();
        }
    }
}
