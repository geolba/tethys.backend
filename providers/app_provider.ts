// // import HttpContextContract from '@ioc:Adonis/Core/HttpContext';
// import { LaravelHash } from './HashDriver/index.js';
import { ApplicationService } from '@adonisjs/core/types';
// import { validator } from '@adonisjs/validator';

export default class AppProvider {
    constructor(protected app: ApplicationService) {}

    public register() {
        // Register your own bindings
       
    }

    public async boot() {
        // IoC container is ready
        await import('../src/extensions.js');
       
        // const hashInstance: typeof hash = this.app.container.make('@adonisjs/core/services/hash');
        // hashInstance.extend('bcrypt', () => {
        //     return new LaravelHash();
        // });

        // this.app.container.resolving('validator', (validator) => {
        //     validator.rule('foo', () => {})

        //     validator.rule('fileExtension', async (value, [extensions], { pointer, arrayExpressionPointer, errorReporter }) => {
        //         const allowedExtensions = extensions.map((ext: string) => ext.toLowerCase());
        //         const uploadedFile = value;

        //         if (!uploadedFile) {
        //             return;
        //         }

        //         const extension = uploadedFile.extname.toLowerCase().replace('.', '');

        //         if (!allowedExtensions.includes(extension)) {
        //             errorReporter.report(
        //                 pointer,
        //                 'fileExtension',
        //                 'Invalid file extension. Only {{ extensions }} files are allowed.',
        //                 arrayExpressionPointer,
        //             );
        //         }
        //     });
    }

    public async ready() {
        // App is ready         
    }

    public async shutdown() {
        // Cleanup, since app is going down
    }
}
