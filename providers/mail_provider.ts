/*
 * @adonisjs/mail
 *
 * (c) AdonisJS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { configProvider } from '@adonisjs/core';
import { RuntimeException } from '@poppinss/utils';
import type { ApplicationService } from '@adonisjs/core/types';

// import { MailManager, Mailer, Message } from '../index.js'
import { MailManager, Mailer, Message } from '@adonisjs/mail';
// import type { MailEvents, MailService } from '../src/types.js'
import type { MailEvents, MailService } from '@adonisjs/mail/types';
import env from '#start/env';
// import { mailPluginEdge } from '@adonisjs/mail';

/**
 * Extended types
 */
declare module '@adonisjs/core/types' {
    export interface ContainerBindings {
        'mail.manager': MailService;
    }
    export interface EventsList extends MailEvents {}
}

/**
 * Mail provider to register mail manager with the container
 */
export default class MailProvider {
    constructor(protected app: ApplicationService) {}

    /**
     * Defines the template engine on the message class to
     * render templates
     */
    protected async defineTemplateEngine() {
        if (this.app.usingEdgeJS) {
            const edge = await import('edge.js');
            Message.templateEngine = {
                render(templatePath, helpers, data) {
                    return edge.default.share(helpers).render(templatePath, data);
                },
            };
            const { mailPluginEdge } = await import('@adonisjs/mail/plugins/edge');
            edge.default.use(mailPluginEdge);
        }
    }

    /**
     * Registering bindings to container
     */
    register() {
        // process.env.SMTP_HOST = 'xhost';
        env.set("SMTP_HOST", 'xhost');
        // this.app.config.set('mail.mailers.smtp.host', 'xhost');

        this.app.container.singleton('mail.manager', async (resolver) => {
            const emitter = await resolver.make('emitter');
            this.app.config.set('mail.mailers.smtp.host', 'xhost');
            // env.set("SMTP_HOST", 'xhost');
            // env.set("SMTP_PORT", '333');
            // Reset or modify environment variables here
           
            const mailConfigProvider = this.app.config.get('mail');
            const config = await configProvider.resolve<any>(this.app, mailConfigProvider);

            const iwas = await config.mailers.smtp();
            // iwas.config.host = 'hhhost';
            // this.app.config.set('mail.mailers.smtp.host', 'xhost');
            // const iwas = await config.mailers.smtp();

            // const smtpConfigProvider = await this.app.config.get('mail.mailers.smtp');
            // const smtpConfig = await configProvider.resolve<any>(this.app, smtpConfigProvider);
            // let test = config.get('mailers.smtp');

            if (!config) {
                throw new RuntimeException('Invalid "config/mail.ts" file. Make sure you are using the "defineConfig" method');
            }

            return new MailManager(emitter, config);
        });

        this.app.container.bind(Mailer, async (resolver) => {
            const mailManager = await resolver.make('mail.manager');
            return mailManager.use();
        });
    }

    /**
     * Invoked automatically when the app is booting
     */
    async boot() {
        await this.defineTemplateEngine();
    }

    /**
     * Cleanup hook
     */
    async shutdown() {
        const mail = await this.app.container.make('mail.manager');
        await mail.closeAll();
    }
}
