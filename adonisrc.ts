import { defineConfig } from '@adonisjs/core/app'

export default defineConfig({
  /*
|--------------------------------------------------------------------------
| Commands
|--------------------------------------------------------------------------
|
| List of ace commands to register from packages. The application commands
| will be scanned automatically from the "./commands" directory.
 
*/
  commands: [
    () => import('@adonisjs/core/commands'),
    () => import('@adonisjs/lucid/commands'),
    () => import('@adonisjs/mail/commands')
  ],
  /*
|--------------------------------------------------------------------------
| Preloads
|--------------------------------------------------------------------------
|
| List of modules to import before starting the application.
|
*/
  preloads: [
    () => import('./start/routes.js'),
    () => import('./start/kernel.js'),
    () => import('#start/validator'),
    () => import('#start/rules/unique'),
    () => import('#start/rules/translated_language'),
    () => import('#start/rules/unique_person'),
    () => import('#start/rules/file_length'),
    () => import('#start/rules/file_scan'),
    () => import('#start/rules/allowed_extensions_mimetypes'),
    () => import('#start/rules/dependent_array_min_length')
  ],
  /*
|--------------------------------------------------------------------------
| Service providers
|--------------------------------------------------------------------------
|
| List of service providers to import and register when booting the
| application
|
*/
  providers: [
    // () => import('./providers/AppProvider.js'),
    () => import('@adonisjs/core/providers/app_provider'),
    () => import('@adonisjs/core/providers/hash_provider'),
    {
      file: () => import('@adonisjs/core/providers/repl_provider'),
      environment: ['repl', 'test'],
    },
    () => import('@adonisjs/session/session_provider'),
    () => import('@adonisjs/core/providers/edge_provider'),
    () => import('@adonisjs/shield/shield_provider'),
    // () => import('@eidellev/inertia-adonisjs'),
    // () => import('@adonisjs/inertia/inertia_provider'),
    () => import('#providers/app_provider'),
    () => import('#providers/inertia_provider'),
    () => import('@adonisjs/lucid/database_provider'),
    () => import('@adonisjs/auth/auth_provider'),
    // () => import('@eidellev/adonis-stardust'),        
    () => import('@adonisjs/redis/redis_provider'),
    () => import('@adonisjs/encore/encore_provider'),
    () => import('@adonisjs/static/static_provider'),
    () => import('#providers/stardust_provider'),
    () => import('#providers/query_builder_provider'),
    () => import('#providers/token_worker_provider'),
    // () => import('#providers/validator_provider'),
    () => import('#providers/drive/provider/drive_provider'),
    // () => import('@adonisjs/core/providers/vinejs_provider'),
    () => import('#providers/vinejs_provider'),
    () => import('@adonisjs/mail/mail_provider')
    //   () => import('#providers/mail_provider'),
  ],
  metaFiles: [
    {
      pattern: 'public/**',
      reloadServer: false,
    },
    {
      pattern: 'resources/views/**/*.edge',
      reloadServer: false,
    },
  ],
  /*
|--------------------------------------------------------------------------
| Tests
|--------------------------------------------------------------------------
|
| List of test suites to organize tests by their type. Feel free to remove
| and add additional suites.
|
*/
  tests: {
    suites: [
      {
        files: ['tests/unit/**/*.spec(.ts|.js)'],
        name: 'unit',
        timeout: 2000,
      },
      {
        files: ['tests/functional/**/*.spec(.ts|.js)'],
        name: 'functional',
        timeout: 30000,
      },
    ],
    forceExit: false,
  },



})
