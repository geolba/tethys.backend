// ./contracts/enums.ts

export enum DatasetTypes {
    analysisdata = 'Analysis',
    measurementdata = 'Measurements',
    monitoring = 'Monitoring',
    remotesensing = 'Remote Sensing',
    gis = 'GIS',
    models = 'Models',
    mixedtype = 'Mixed Type',
    vocabulary = 'Vocabulary',
}

export enum ServerStates {
    deleted = 'deleted',
    inprogress = 'inprogress',
    published = 'published',
    released = 'released',
    editor_accepted = 'editor_accepted',
    approved = 'approved',
    rejected_reviewer = 'rejected_reviewer',
    rejected_editor = 'rejected_editor',
    reviewed = 'reviewed',
}

// for table dataset_titles
export enum TitleTypes {
    Main = 'Main',
    Sub = 'Sub',
    Alternative = 'Alternative',
    Translated = 'Translated',
    Other = 'Other',
}

// for table dataset_abstracts
export enum DescriptionTypes {
    Abstract = 'Abstract',
    Methods = 'Methods',
    Series_information = 'Series_information',
    Technical_info = 'Technical_info',
    Translated = 'Translated',
    Other = 'Other',
}

export enum PersonNameTypes {
    Organizational = 'Organizational',
    Personal = 'Personal',
}

export enum PersonRoles {
    author = 'author',
    contributor = 'contributor',
    other = 'other',
}

export enum ContributorTypes {
    ContactPerson = 'ContactPerson',
    DataCollector = 'DataCollector',
    DataCurator = 'DataCurator',
    DataManager = 'DataManager',
    Distributor = 'Distributor',
    Editor = 'Editor',
    HostingInstitution = 'HostingInstitution',
    Producer = 'Producer',
    ProjectLeader = 'ProjectLeader',
    ProjectManager = 'ProjectManager',
    ProjectMember = 'ProjectMember',
    RegistrationAgency = 'RegistrationAgency',
    RegistrationAuthority = 'RegistrationAuthority',
    RelatedPerson = 'RelatedPerson',
    Researcher = 'Researcher',
    ResearchGroup = 'ResearchGroup',
    RightsHolder = 'RightsHolder',
    Sponsor = 'Sponsor',
    Supervisor = 'Supervisor',
    WorkPackageLeader = 'WorkPackageLeader',
    Other = 'Other',
}

export enum SubjectTypes {
    uncontrolled = 'uncontrolled',
    geoera = 'GeoEra',
}

export enum ReferenceIdentifierTypes {
    DOI = 'DOI',
    Handle = 'Handle',
    ISBN = 'ISBN',
    ISSN = 'ISSN',
    URL = 'URL',
    URN = 'URN',
}

export enum RelationTypes {
    IsSupplementTo = 'IsSupplementTo',
    IsSupplementedBy = 'IsSupplementedBy',
    IsContinuedBy = 'IsContinuedBy',
    Continues = 'Continues',
    IsNewVersionOf = 'IsNewVersionOf',
    IsPartOf = 'IsPartOf',
    HasPart = 'HasPart',
    Compiles = 'Compiles',
    IsVariantFormOf = 'IsVariantFormOf',
}

export enum IdentifierTypes {
    doi = 'doi',
    handle = 'handle',
    isbn = 'isbn',
    issn = 'issn',
    url = 'url',
    urn = 'urn',
}

export enum TotpState {
    STATE_DISABLED = 0,
    STATE_CREATED = 1,
    STATE_ENABLED = 2,
}
