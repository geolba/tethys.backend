import { test } from '@japa/runner';
import supertest from 'supertest';
import db from '@adonisjs/lucid/services/db';
import Dataset from '#models/dataset';
// import server from '@adonisjs/core/services/server'; // Import the server instance
// import { Server } from '@adonisjs/core/http';
import User from '#models/user';
import Role from '#models/role';
import Permission from '#models/permission';
import { TestContext } from '@japa/runner/core';
const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}`

test.group('DatasetController', (group) => {
    // Write your test here
    // beforeEach(async () => {
    //     await Database.beginGlobalTransaction();
    // });
    // In the following example, we start a global transaction before all the tests and roll back it after the tests.
    group.each.setup(async () => {
        await db.beginGlobalTransaction();
        return () => db.rollbackGlobalTransaction();
    });
    // group.setup(async  () => {
    //    server = await supertest(BASE_URL);
    // });



    test('should render dataset release page', async ({ assert }: TestContext) => {
        var testAgent = supertest(BASE_URL);

        // const user = await loginUser(agent);
        const user = await User.create({
            login: 'test',
            email: 'alice@email.com',
            password: 'password',
        });
       
        const role = await Role.create({
            name: 'administrator',
            display_name: 'admin',
            description: 'User has access to all system functionality'
        });
        await user.related('roles').attach([role.id]);

    
        const permission = await Permission.create({
            name: 'dataset-edit',
            display_name: 'edit dataset',
            description: 'allow role to edit datasets'
        });
        await role.related('permissions').attach([permission.id]);

        const dataset = new Dataset();
        dataset.type = 'analysisdata';
        dataset.creating_corporation = 'Tethys RDR';
        dataset.language = 'de';
        dataset.server_state = 'inprogress'; // Set the desired server state here
        // await dataset.save();
        await user.related('datasets').save(dataset);

        // Perform the login request to establish the session
        const loginResponse = await testAgent
            .post('/app/login')
            // .field('email', user.email)
            // .field('password', user.password)
            // .set('Content-Type', 'application/x-www-form-urlencoded')
            .send({ email: user.email, password: 'password' })
            .set('Content-Type', 'application/json')
            .expect(302); // Assuming the login endpoint redirects, if succesful
        // Extract the session cookies from the login response
        assert.exists(loginResponse.headers['set-cookie']);
        // const cookie = loginResponse.headers['set-cookie'];

        // // const response = await supertest(server.instance).get(`/submitter/dataset/${dataset.id}/release`).expect(302); //302 if authenticated, else 200
        // const response = await testAgent
        //     .get(`/submitter/dataset/${dataset.id}/release`)
        //     .set('Cookie', cookie)
        //     .set('X-Requested-With', 'XMLHttpRequest') // Set the X-Requested-With header to mimic an AJAX request
        //     .set('X-Inertia', 'true')
        //     // .set('X-Inertia-Version', '97c0899ca6e04e77a69f1ad5320e4ebe')
        //     .set('Accept', 'text/html, application/xhtml+xml')
        //     .set('Accept-Encoding', 'gzip, deflate, br')
        //     .set('Connection', 'keep-alive')
        //     .set(
        //         'User-Agent',
        //         'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
        //     )
        //     // .set('Host', 'localhost:4001')
        //     // .set('Referer', 'http://localhost:3333/submitter/dataset')
        //     // .set('Authorization', `Bearer ${authToken}`) // Set the Authorization header with the authentication token            .

        //     .expect(200);

        // // assert.equal(response.statusCode, 302);

        // // Add more assertions based on the expected behavior
        // // assert.equal(response.header('X-Inertia'), 'true');
        // // assert.equal(response.header('Content-Type'), 'application/json');
        // assert.equal(response.headers['x-inertia'], 'true');
        // assert.equal(response.headers['content-type'], 'application/json; charset=utf-8');

        // const responseData = response.body as { component: string; props: { dataset: any }; url: string };
        // // assert.equal(responseData.component, 'DatasetReleasePage');
        // assert.equal(responseData.props.dataset.id, dataset.id);
    });
});
