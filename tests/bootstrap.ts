/**
 * File source: https://bit.ly/3ukaHTz
 *
 * Feel free to let us know via PR, if you find something broken in this contract
 * file.
 */

// import type { Config } from '@japa/runner';
import type { Config } from '@japa/runner/types';
// import TestUtils from '@ioc:Adonis/Core/TestUtils';
import testUtils from '@adonisjs/core/services/test_utils';
// import { assert, runFailedTests, specReporter, apiClient } from '@japa/preset-adonis';

import { assert } from '@japa/assert';
import { apiClient } from '@japa/api-client';
import { pluginAdonisJS } from '@japa/plugin-adonisjs';
import app from '@adonisjs/core/services/app';

/*
|--------------------------------------------------------------------------
| Japa Plugins
|--------------------------------------------------------------------------
|
| Japa plugins allows you to add additional features to Japa. By default
| we register the assertion plugin.
|
| Feel free to remove existing plugins or add more.
|
*/
// export const plugins: Required<Config>['plugins'] = [assert(), runFailedTests(), apiClient()];
export const plugins: Config['plugins'] = [assert(), apiClient(), pluginAdonisJS(app)];

/*
|--------------------------------------------------------------------------
| Japa Reporters
|--------------------------------------------------------------------------
|
| Japa reporters displays/saves the progress of tests as they are executed.
| By default, we register the spec reporter to show a detailed report
| of tests on the terminal.
|
*/
// export const reporters: Required<Config>['reporters'] = [specReporter()];
export const reporters: Required<Config>['reporters'] = {
    activated: ['spec'],
};

/*
|--------------------------------------------------------------------------
| Runner hooks
|--------------------------------------------------------------------------
|
| Runner hooks are executed after booting the AdonisJS app and
| before the test files are imported.
|
| You can perform actions like starting the HTTP server or running migrations
| within the runner hooks
|
*/
export const runnerHooks: Pick<Required<Config>, 'setup' | 'teardown'> = {
    setup: [
        // () => testUtils.ace().loadCommands(),
        () => testUtils.db().migrate(),
        // () => testUtils.httpServer().start(),
    ],
    teardown: [],
};

/*
|--------------------------------------------------------------------------
| Configure individual suites
|--------------------------------------------------------------------------
|
| The configureSuite method gets called for every test suite registered
| within ".adonisrc.json" file.
|
| You can use this method to configure suites. For example: Only start
| the HTTP server when it is a functional suite.
*/
export const configureSuite: Required<Config>['configureSuite'] = (suite) => {
    if (suite.name === 'functional') {
        suite.setup(() => testUtils.httpServer().start());
    }
};
