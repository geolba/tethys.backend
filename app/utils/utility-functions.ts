export function sum(a: number, b: number): number {
    return a + b;
}

export function getDomain(host: string): string {
    // $myhost = strtolower(trim($host));
    let myHost: string = host.trim().toLocaleLowerCase();
    // $count = substr_count($myhost, '.');
    const count: number = myHost.split(',').length - 1;

    if (count == 2) {
        const words = myHost.split('.');
        if (words[1].length > 3) {
            myHost = myHost.split('.', 2)[1];
        }
    } else if (count > 2) {
        myHost = getDomain(myHost.split('.', 2)[1]);
    }
    myHost = myHost.replace(new RegExp(/^.*:\/\//i, 'g'), '');
    return myHost;
}

export function preg_match(regex: RegExp, str: string) {
    const result: boolean = regex.test(str);
    return result;
}
