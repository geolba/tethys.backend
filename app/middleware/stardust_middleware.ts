import type { HttpContext } from '@adonisjs/core/http';
import type { NextFn } from '@adonisjs/core/types/http';


declare global {
    function myFunction(): boolean;
    var myVariable: number;

    interface StardustData {
        pathname?: string;
        namedRoutes?: Record<string, string>;
    }
    var stardust: StardustData;
}

declare global {}
export default class StardustMiddleware {
    async handle(ctx: HttpContext, next: NextFn): Promise<void> {
        /**
         * Middleware logic goes here (before the next call)
         */

        // Check if the request is an API request
        if (!ctx.request.url().startsWith('/api')) {
            // Middleware logic for non-API requests
            const { pathname } = new URL(ctx.request.completeUrl()); // '/', '/app/login'
            globalThis.myFunction = () => {
                return true;
            };
            globalThis.myVariable = 1;

            globalThis.stardust = {
                ...globalThis.stardust,
                pathname,
            };

            /**
             * Call next method in the pipeline and return its output
             */
            await next();
        } else {
            // Skip middleware for API requests
            await next();
        }
    }
}

