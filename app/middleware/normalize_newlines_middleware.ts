/* 
* This middleware class normalizes newlines in the request input data by replacing 
* all occurrences of '\r\n' with '\n' recursively for strings, arrays, and objects.
*/
import type { HttpContext } from '@adonisjs/core/http';
import type { NextFn } from '@adonisjs/core/types/http';

export default class NormalizeNewlinesMiddleware {
    async handle(ctx: HttpContext, next: NextFn) {
        // Function to recursively normalize newlines
        const normalizeNewlines = (input: any): any => {
            if (typeof input === 'string') {
                return input.replace(/\r\n/g, '\n');
            } else if (Array.isArray(input)) {
                return input.map((item) => normalizeNewlines(item));
            } else if (typeof input === 'object' && input !== null) {
                for (const key in input) {
                    input[key] = normalizeNewlines(input[key]);
                }
                return input;
            }
            return input;
        };
        /**
         * Middleware logic goes here (before the next call)
         */
        // console.log(ctx)
        // Get all request input
        const input = ctx.request.all();

        // Normalize newlines in text inputs
        const normalizedInput = normalizeNewlines(input);

        // Replace request input with normalized data
        ctx.request.updateBody(normalizedInput);

        /**
         * Call next method in the pipeline and return its output
         */
        const output = await next();
        return output;
    }
}
