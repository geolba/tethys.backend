import vine, { SimpleMessagesProvider } from '@vinejs/vine';

/**
 * Validates the role's creation action
 * node ace make:validator role
 */
export const createRoleValidator = vine.compile(
    vine.object({
        name: vine
            .string()
            .isUnique({ table: 'roles', column: 'name' })
            .trim()
            .minLength(3)
            .maxLength(255)
            .regex(/^[a-zA-Z0-9]+$/), //Must be alphanumeric with hyphens or underscores
        display_name: vine
            .string()
            .isUnique({ table: 'roles', column: 'display_name' })
            .trim()
            .minLength(3)
            .maxLength(255)
            .regex(/^[a-zA-Z0-9]+$/),
        description: vine.string().trim().escape().minLength(3).maxLength(255).optional(),
        permissions: vine.array(vine.number()).minLength(1), // define at least one permission for the new role
    }),
);

export const updateRoleValidator = vine.withMetaData<{ roleId: number }>().compile(
    vine.object({
        name: vine
            .string()
            // .unique(async (db, value, field) => {
            //     const result = await db.from('roles').select('id').whereNot('id', field.meta.roleId).where('name', value).first();
            //     return result.length ? false : true;
            // })
            .isUnique({
                table: 'roles',
                column: 'name',
                whereNot: (field) => field.meta.roleId,
            })
            .trim()
            .minLength(3)
            .maxLength(255),

        description: vine.string().trim().escape().minLength(3).maxLength(255).optional(),
        permissions: vine.array(vine.number()).minLength(1), // define at least one permission for the new role
    }),
);

let messagesProvider = new SimpleMessagesProvider({
    // Applicable for all fields
    'required': 'The {{ field }} field is required',
    'unique': '{{ field }} must be unique, and this value is already taken',
    'string': 'The value of {{ field }} field must be a string',
    'email': 'The value is not a valid email address',

    // 'contacts.0.email.required': 'The primary email of the contact is required',
    // 'contacts.*.email.required': 'Contact email is required',
    'permissions.minLength': 'at least {{min }} permission must be defined',
    'permissions.*.number': 'Define permissions as valid numbers',
});

createRoleValidator.messagesProvider = messagesProvider;
updateRoleValidator.messagesProvider = messagesProvider;
