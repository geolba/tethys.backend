import vine from '@vinejs/vine';

// public schema = schema.create({
//     email: schema.string({ trim: true }, [
//         rules.email(),
//         // rules.unique({ table: 'accounts', column: 'email' })
//     ]),
//     password: schema.string({}, [rules.minLength(6)]),
// });

/**
 * Validates the role's creation action
 * node ace make:validator role
 */
export const authValidator = vine.compile(
    vine.object({
        email: vine.string().maxLength(255).email().normalizeEmail(),
        password: vine.string().trim().minLength(6),
    }),
);
