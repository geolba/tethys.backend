import vine, { SimpleMessagesProvider } from '@vinejs/vine';

/**
 * Validates the role's creation action
 * node ace make:validator user
 */
export const createUserValidator = vine.compile(
    vine.object({
        login: vine
            .string()
            .trim()
            .minLength(3)
            .maxLength(20)
            .isUnique({ table: 'accounts', column: 'login' })
            .regex(/^[a-zA-Z0-9]+$/), //Must be alphanumeric with hyphens or underscores
        first_name: vine.string().trim().minLength(3).maxLength(255),
        last_name: vine.string().trim().minLength(3).maxLength(255),
        email: vine.string().maxLength(255).email().normalizeEmail().isUnique({ table: 'accounts', column: 'email' }),
        password: vine.string().confirmed().trim().minLength(3).maxLength(60),
        roles: vine.array(vine.number()).minLength(1), // define at least one role for the new user
    }),
);

/**
 * Validates the role's update action
 * node ace make:validator user
 */
export const updateUserValidator = vine.withMetaData<{ objId: number }>().compile(
    vine.object({
        login: vine
            .string()
            .trim()
            .minLength(3)
            .maxLength(20)
            .isUnique({ table: 'accounts', column: 'login', whereNot: (field) => field.meta.objId }),
            // .regex(/^[a-zA-Z0-9]+$/), //Must be alphanumeric with hyphens or underscores
        first_name: vine.string().trim().minLength(3).maxLength(255),
        last_name: vine.string().trim().minLength(3).maxLength(255),
        email: vine
            .string()
            .maxLength(255)
            .email()
            .normalizeEmail()
            .isUnique({ table: 'accounts', column: 'email', whereNot: (field) => field.meta.objId }),
        password: vine.string().confirmed().trim().minLength(3).maxLength(60).optional(),
        roles: vine.array(vine.number()).minLength(1), // define at least one role for the new user
    }),
);

let messagesProvider = new SimpleMessagesProvider({
    // Applicable for all fields
    'required': 'The {{ field }} field is required',
    'unique': '{{ field }} must be unique, and this value is already taken',
    'string': 'The value of {{ field }} field must be a string',
    'email': 'The value is not a valid email address',
    'minLength': '{{ field }} must be at least {{ min }} characters long',
    'maxLength': '{{ field }} must be less then {{ max }} characters long',
    'confirmed': 'Oops! The confirmation of {{ field }} is not correct. Please double-check and ensure they match.',
    'roles.minLength': 'at least {{ min }} role must be defined',
    'roles.*.number': 'Define roles as valid numbers',
});

createUserValidator.messagesProvider = messagesProvider;
updateUserValidator.messagesProvider = messagesProvider;
