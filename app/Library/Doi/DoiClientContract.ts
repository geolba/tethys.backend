// import ResumptionToken from './ResumptionToken';
import { AxiosResponse } from 'axios';

export default interface DoiClientContract {
    username: string;
    password: string;
    serviceUrl: string;
    // prefix: string;
    // base_domain: string;
    registerDoi(doiValue: string, xmlMeta: string, landingPageUrl: string): Promise<AxiosResponse<any>>;
    // get(key: string): Promise<ResumptionToken | null>;
    // set(token: ResumptionToken): Promise<string>;
}