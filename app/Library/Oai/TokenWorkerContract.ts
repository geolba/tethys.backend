import ResumptionToken from './ResumptionToken.js';

export default abstract class TokenWorkerContract {
    abstract ttl: number;
    abstract isConnected: boolean;
    abstract connect(): void;
    abstract close(): void;
    abstract get(key: string): Promise<ResumptionToken | null>;
    abstract set(token: ResumptionToken): Promise<string>;
}

