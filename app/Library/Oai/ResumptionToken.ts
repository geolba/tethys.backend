export default class ResumptionToken {
    private _documentIds: number[] = [];
    private _metadataPrefix = '';
    private _resumptionId = '';
    private _startPosition = 0;
    private _totalIds = 0;

    get key(): string {
        return this.metadataPrefix + this.startPosition + this.totalIds;
    }

    get documentIds(): number[] {
        return this._documentIds;
    }

    set documentIds(idsToStore: number | number[]) {
        this._documentIds = Array.isArray(idsToStore) ? idsToStore : [idsToStore];
    }

    get metadataPrefix(): string {
        return this._metadataPrefix;
    }

    set metadataPrefix(value: string) {
        this._metadataPrefix = value;
    }

    get resumptionId(): string {
        return this._resumptionId;
    }

    set resumptionId(resumptionId: string) {
        this._resumptionId = resumptionId;
    }

    get startPosition(): number {
        return this._startPosition;
    }

    set startPosition(startPosition: number) {
        this._startPosition = startPosition;
    }

    get totalIds(): number {
        return this._totalIds;
    }

    set totalIds(totalIds: number) {
        this._totalIds = totalIds;
    }
}
