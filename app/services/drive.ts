// import app from './app.js';
import DriveManager from "#providers/drive/src/drive_manager";
import app from "@adonisjs/core/services/app";

let drive: DriveManager;
/**
 * Returns a singleton instance of the router class from
 * the container
 */
await app.booted(async () => {
    drive = await app.container.make(DriveManager);
});
export { drive as default };