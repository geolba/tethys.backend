import type { HttpContext } from '@adonisjs/core/http';
import License from '#models/license';

export default class LicenseController {
    public async index({ auth, inertia }: HttpContext) {
        const direction = 'asc'; // or 'desc'
        const licenses = await License.query().orderBy('sort_order', direction).exec();

        return inertia.render('Admin/License/Index', {
            licenses: licenses,
            can: {
                edit: await auth.user?.can(['settings']),
            },
        });
    }

    public async down({ request, response }: HttpContext) {
        const id = request.param('id');
        const license = await License.findOrFail(id);
        license.active = false;
        await license.save();

        // session.flash({ message: 'person has been deactivated!' });
        return response.flash('License has been deactivated!', 'message').toRoute('settings.license.index')
    }

    public async up({ request, response }: HttpContext) {
        const id = request.param('id');
        const license = await License.findOrFail(id);
        license.active = true;
        await license.save();

        // session.flash({ message: 'person has been activated!' });
        return response.flash('License has been activated!', 'message').toRoute('settings.license.index');
    }

    // public async edit({ request, inertia }: HttpContext) {
    //     const id = request.param('id');
    //     const license = await License.query().where('id', id).firstOrFail();

    //     // const permissions = await Permission.query().pluck('name', 'id');
    //     // // const userHasRoles = user.roles;
    //     // const rolerHasPermissions = await role.related('permissions').query().orderBy('name').pluck('id');

    //     return inertia.render('Admin/License/Edit', {
    //         // permissions: permissions,
    //         license: license,
    //         // roleHasPermissions: Object.keys(rolerHasPermissions).map((key) => rolerHasPermissions[key]), //convert object to array with role ids
    //     });
    // }
}
