import type { HttpContext } from '@adonisjs/core/http';

export default class HomeController {
    public async index({}: HttpContext) {}

    public async create({}: HttpContext) {}

    public async store({}: HttpContext) {}

    public async show({}: HttpContext) {}

    public async edit({}: HttpContext) {}

    public async update({}: HttpContext) {}

    public async destroy({}: HttpContext) {}
}
