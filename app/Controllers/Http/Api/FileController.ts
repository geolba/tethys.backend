import type { HttpContext } from '@adonisjs/core/http';
import File from '#models/file';
import { StatusCodes } from 'http-status-codes';
import * as fs from 'fs';
import * as path from 'path';

// node ace make:controller Author
export default class FileController {
    // @Get("download/:id")
    public async findOne({ response, params }: HttpContext) {
        const id = params.id;
        const file = await File.findOrFail(id);
        // const file = await File.findOne({
        //     where: { id: id },
        // });
        if (file) {
            const filePath = '/storage/app/public/' + file.pathName;
            const ext = path.extname(filePath);
            const fileName = file.label + ext;
            try {
                fs.accessSync(filePath, fs.constants.R_OK); //| fs.constants.W_OK);
                // console.log("can read/write:", path);
               
                response
                    .header('Cache-Control', 'no-cache private')
                    .header('Content-Description', 'File Transfer')
                    .header('Content-Type', file.mimeType)
                    .header('Content-Disposition', 'inline; filename=' + fileName)
                    .header('Content-Transfer-Encoding', 'binary')
                    .header('Access-Control-Allow-Origin', '*')
                    .header('Access-Control-Allow-Methods', 'GET,POST');
                
                response.status(StatusCodes.OK).download(filePath);               
            } catch (err) {
                // console.log("no access:", path);
                response.status(StatusCodes.NOT_FOUND).send({
                    message: `File with id ${id} doesn't exist on file server`,
                });
            }

            // res.status(StatusCodes.OK).sendFile(filePath, (err) => {
            //     // res.setHeader("Content-Type", "application/json");
            //     // res.removeHeader("Content-Disposition");
            //     res.status(StatusCodes.NOT_FOUND).send({
            //         message: `File with id ${id} doesn't exist on file server`,
            //     });
            // });
        } else {
            response.status(StatusCodes.NOT_FOUND).send({
                message: `Cannot find File with id=${id}.`,
            });
        }
    }
}
