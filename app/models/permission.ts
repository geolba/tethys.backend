import { column, manyToMany, SnakeCaseNamingStrategy } from '@adonisjs/lucid/orm';
import { DateTime } from 'luxon';
import dayjs from 'dayjs';
import Role from '#models/role';
import BaseModel from './base_model.js';
import type { ManyToMany } from "@adonisjs/lucid/types/relations";

export default class Permission extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'permissions';
    public static selfAssignPrimaryKey = false;

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public role_id: number;

    @column({})
    public display_name: string;

    @column({})
    public name: string;

    @column({})
    public description: string;

    @column.dateTime({
        serialize: (value: Date | null) => {
            return value ? dayjs(value).format('MMMM D YYYY HH:mm a') : value;
        },
        autoCreate: true,
    })
    public created_at: DateTime;

    @column.dateTime({
        serialize: (value: Date | null) => {
            return value ? dayjs(value).format('MMMM D YYYY HH:mm a') : value;
        },
        autoCreate: true,
        autoUpdate: true,
    })
    public updated_at: DateTime;

    // @beforeCreate()
    // @beforeUpdate()
    // public static async resetDate(role) {
    //     role.created_at = this.formatDateTime(role.created_at);
    //     role.updated_at = this.formatDateTime(role.updated_at);
    // }

    // public static boot() {
    //   super.boot()

    //   this.before('create', async (_modelInstance) => {
    //     _modelInstance.created_at = this.formatDateTime(_modelInstance.created_at)
    //     _modelInstance.updated_at = this.formatDateTime(_modelInstance.updated_at)
    //   })
    //   this.before('update', async (_modelInstance) => {
    //     _modelInstance.created_at = this.formatDateTime(_modelInstance.created_at)
    //     _modelInstance.updated_at = this.formatDateTime(_modelInstance.updated_at)
    //   })
    // }

    // private static formatDateTime(datetime) {
    //     let value = new Date(datetime);
    //     return datetime
    //         ? value.getFullYear() +
    //               '-' +
    //               (value.getMonth() + 1) +
    //               '-' +
    //               value.getDate() +
    //               ' ' +
    //               value.getHours() +
    //               ':' +
    //               value.getMinutes() +
    //               ':' +
    //               value.getSeconds()
    //         : datetime;
    // }

    // @belongsTo(() => Role)
    // public role: BelongsTo<typeof Role>;

    @manyToMany(() => Role, {
        pivotForeignKey: 'permission_id',
        pivotRelatedForeignKey: 'role_id',
        pivotTable: 'role_has_permissions',
    })
    public roles: ManyToMany<typeof Role>;
}
