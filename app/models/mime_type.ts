import { column, SnakeCaseNamingStrategy } from '@adonisjs/lucid/orm';
import BaseModel from './base_model.js';
import { DateTime } from 'luxon';

export default class MimeType extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'mime_types';
    public static fillable: string[] = ['name', 'file_extension', 'enabled'];

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public name: string;

    @column({})
    public file_extension: string;

    @column({})
    public enabled: boolean;

    @column.dateTime({
        autoCreate: true,
    })
    public created_at: DateTime;

    @column.dateTime({
        autoCreate: true,
        autoUpdate: true,
    })
    public updated_at: DateTime;

    // @hasMany(() => Collection, {
    //     foreignKey: 'role_id',
    // })
    // public collections: HasMany<typeof Collection>;
}
