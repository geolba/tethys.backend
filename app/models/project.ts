import { column, SnakeCaseNamingStrategy } from '@adonisjs/lucid/orm';
import { DateTime } from 'luxon';
import BaseModel from './base_model.js';

export default class Project extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'projects';
    public static selfAssignPrimaryKey = false;

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public label: string;

    @column({})
    public name: string;

    @column({})
    public description: string;

    @column.dateTime({
        autoCreate: true,
    })
    public created_at: DateTime;

    @column.dateTime({
        autoCreate: true,
        autoUpdate: true,
    })
    public updated_at: DateTime;
}
