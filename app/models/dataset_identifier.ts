import { column, SnakeCaseNamingStrategy, belongsTo } from '@adonisjs/lucid/orm';
import { DateTime } from 'luxon';
import Dataset from './dataset.js';
import BaseModel from './base_model.js';
import type { BelongsTo } from "@adonisjs/lucid/types/relations";

export default class DatasetIdentifier extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'dataset_identifiers';
    public static fillable: string[] = ['value', 'label', 'type', 'relation'];

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public dataset_id: number;

    @column({})
    public type: string;

    @column({})
    public status: string;

    @column({})
    public value: string;

    @column.dateTime({
        autoCreate: true,
    })
    public created_at?: DateTime;

    @column.dateTime({
        autoCreate: true,
        autoUpdate: true,
    })
    public updated_at?: DateTime;

    @belongsTo(() => Dataset, {
        foreignKey: 'dataset_id',
    })
    public dataset: BelongsTo<typeof Dataset>;

    // // Specify the relationships to touch when this model is updated
    // public static get touches() {
    //     return ['dataset'];
    // }
}
