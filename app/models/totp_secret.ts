import { column, BaseModel, SnakeCaseNamingStrategy, belongsTo } from '@adonisjs/lucid/orm';
import User from './user.js';
import { DateTime } from 'luxon';
import dayjs from 'dayjs';
// import Encryption from '@ioc:Adonis/Core/Encryption';
import encryption from '@adonisjs/core/services/encryption';
import type { BelongsTo } from "@adonisjs/lucid/types/relations";

export default class TotpSecret extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static table = 'totp_secrets';
    // public static fillable: string[] = ['value', 'label', 'type', 'relation'];
   
    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public user_id: number;

    // @column()
    // public twoFactorSecret: string;
    @column({
        serializeAs: null,
        consume: (value: string) => (value ? JSON.parse(encryption.decrypt(value) ?? '{}') : null),
        prepare: (value: string) => encryption.encrypt(JSON.stringify(value)),
    })
    public twoFactorSecret?: string | null;

    // serializeAs: null removes the model properties from the serialized output.
    @column({
        serializeAs: null,
        consume: (value: string) => (value ? JSON.parse(encryption.decrypt(value) ?? '[]') : []),
        prepare: (value: string[]) => encryption.encrypt(JSON.stringify(value)),
    })
    public twoFactorRecoveryCodes?: string[] | null;

    @column({})
    public state: number;

    @column.dateTime({
        serialize: (value: Date | null) => {
            // return value ? moment(value).format('MMMM Do YYYY, HH:mm:ss') : value;
            return value ? dayjs(value).format('MMMM D YYYY HH:mm a') : value;
        },
        autoCreate: true,
    })
    public created_at: DateTime;

    @column.dateTime({
        serialize: (value: Date | null) => {
            return value ? dayjs(value).format('MMMM D YYYY HH:mm a') : value;
        },
        autoCreate: true,
        autoUpdate: true,
    })
    public updated_at: DateTime;

    @belongsTo(() => User, {
        foreignKey: 'user_id',
    })
    public user: BelongsTo<typeof User>;

}
