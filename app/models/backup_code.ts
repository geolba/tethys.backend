import BaseModel from './base_model.js';
import { column, SnakeCaseNamingStrategy, belongsTo } from '@adonisjs/lucid/orm';
import User from './user.js';
import type { BelongsTo } from '@adonisjs/lucid/types/relations';
import db from '@adonisjs/lucid/services/db';
import hash from '@adonisjs/core/services/hash';

export default class BackupCode extends BaseModel {
    public static table = 'backupcodes';
    public static namingStrategy = new SnakeCaseNamingStrategy();

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public user_id: number;

    @column({
        // serializeAs: null,
        // consume: (value: string) => (value ? JSON.parse(encryption.decrypt(value) ?? '{}') : null),
        // prepare: (value: string) => encryption.encrypt(JSON.stringify(value)),
    })
    public code: string;

    @column({})
    public used: boolean;

    @belongsTo(() => User, {
        foreignKey: 'user_id',
    })
    public user: BelongsTo<typeof User>;

    // public static async getBackupCodes(user: User): Promise<BackupCode[]> {
    //     return await db.from(this.table).select('id', 'user_id', 'code', 'used').where('user_id', user.id);
    // }

    public static async deleteCodes(user: User): Promise<void> {
        await db.from(this.table).where('user_id', user.id).delete();
    }

    public static async deleteCodesByUserId(uid: string): Promise<void> {
        await db.from(this.table).where('user_id', uid).delete();
    }

    // Method to verify password
    public async verifyCode(plainCode: string) {
        return await hash.verify(this.code, plainCode);
    }
}
