import BaseModel from './base_model.js';
import { column } from '@adonisjs/lucid/orm';

export default class AppConfig extends BaseModel {
    public static table = 'appconfigs'; // Specify the table name if it differs from the model name

    @column({ isPrimary: true })
    public id: number;

    @column()
    public appid: string;

    @column()
    public configkey: string;

    @column()
    public configvalue: string | null;

    @column()
    public type: number;

    @column()
    public lazy: number;

    // async function setConfig(key: string, value: string) {
    //     await this.updateOrCreate({ key }, { value })
    //   }

    // async function getConfig(key: string) {
    //     const config = await this.findBy('key', key)
    //     return config ? config.value : null
    //   }
}
