import { column, SnakeCaseNamingStrategy } from '@adonisjs/lucid/orm';
import BaseModel from './base_model.js';
// import { DateTime } from 'luxon';

export default class Language extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'languages';
    public static selfAssignPrimaryKey = false;

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public part2_b: string;

    @column({})
    public part2_T: string;

    @column({})
    public description: string;

    @column({})
    public part1: string;

    @column({})
    public type: string;

    @column({})
    public ref_name: string;

    @column({})
    public active: boolean;
}
