import { column, SnakeCaseNamingStrategy, manyToMany, belongsTo } from '@adonisjs/lucid/orm';
import Dataset from './dataset.js';
import BaseModel from './base_model.js';
import CollectionRole from './collection_role.js';
import type { ManyToMany } from "@adonisjs/lucid/types/relations";
import type { BelongsTo } from "@adonisjs/lucid/types/relations";

export default class Collection extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'collections';
    public static fillable: string[] = ['name', 'number', 'role_id'];

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public document_id: number;

    @column({})
    public role_id?: number;

    @column({})
    public number?: string;

    @column({})
    public name: string;

    @column({})
    public oai_subset?: string;

    @column({})
    public parent_id?: number;

    @column({})
    public visible: boolean;

    @column({})
    public visible_publish: boolean;

    @manyToMany(() => Dataset, {
        pivotForeignKey: 'collection_id',
        pivotRelatedForeignKey: 'document_id',
        pivotTable: 'link_documents_collections',
    })
    public datasets: ManyToMany<typeof Dataset>;

    @belongsTo(() => CollectionRole, {
        foreignKey: 'role_id',
    })
    public collectionRole: BelongsTo<typeof CollectionRole>;
}
