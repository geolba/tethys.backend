import { column, BaseModel, belongsTo, SnakeCaseNamingStrategy } from '@adonisjs/lucid/orm';
import File from './file.js';
import type { BelongsTo } from "@adonisjs/lucid/types/relations";

export default class HashValue extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    // public static primaryKey = 'file_id,type';
    public static table = 'file_hashvalues';

    // static get primaryKey () {
    //     return 'type, value'
    //   }

    public static get incrementing() {
        return false;
    }

    // @column({
    // 	isPrimary: true,
    // })
    // public id: number;

    // Foreign key is still on the same model
    @column({ isPrimary: true })
    public file_id: number;

    @column({ isPrimary: true })
    public type: string;

    @column()
    public value: string;

    @belongsTo(() => File)
    public file: BelongsTo<typeof File>;
}
