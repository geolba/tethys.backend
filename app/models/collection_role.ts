import { column, SnakeCaseNamingStrategy, hasMany } from '@adonisjs/lucid/orm';
import BaseModel from './base_model.js';
import Collection from './collection.js';
import type { HasMany } from "@adonisjs/lucid/types/relations";

export default class CollectionRole extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'collections_roles';
    public static fillable: string[] = ['name', 'oai_name', 'visible'];

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public name: string;

    @column({})
    public oai_name?: string;

    @column({})
    public position: number;

    @column({})
    public visible: boolean;

    @column({})
    public visible_frontdoor: boolean;

    @column({})
    public visible_oai: boolean;

    @hasMany(() => Collection, {
        foreignKey: 'role_id',
    })
    public collections: HasMany<typeof Collection>;
}
