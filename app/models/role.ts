import { column, SnakeCaseNamingStrategy, manyToMany, beforeCreate, beforeUpdate } from '@adonisjs/lucid/orm';
import BaseModel from './base_model.js';
import { DateTime } from 'luxon';
// import moment from 'moment';
import dayjs from 'dayjs';
import User from './user.js';
import Permission from '#models/permission';
import type { ManyToMany } from "@adonisjs/lucid/types/relations";

export default class Role extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'roles';
    public static selfAssignPrimaryKey = false;

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public display_name: string;

    @column({})
    public name: string;

    @column({})
    public description: string;

    @column.dateTime({
        serialize: (value: Date | null) => {
            // return value ? moment(value).format('MMMM Do YYYY, HH:mm:ss') : value;
            return value ? dayjs(value).format('MMMM D YYYY HH:mm a') : value;
        },
        autoCreate: true,
    })
    public created_at: DateTime;

    @column.dateTime({
        serialize: (value: Date | null) => {
            return value ? dayjs(value).format('MMMM D YYYY HH:mm a') : value;
        },
        autoCreate: true,
        autoUpdate: true,
    })
    public updated_at: DateTime;

    @beforeCreate()
    @beforeUpdate()
    public static async resetDate(role: Role) {
        role.created_at = this.formatDateTime(role.created_at);
        role.updated_at = this.formatDateTime(role.updated_at);
    }

    // public static boot() {
    // 	super.boot();

    // 	this.before('create', async (_modelInstance) => {
    // 		_modelInstance.created_at = this.formatDateTime(_modelInstance.created_at);
    // 		_modelInstance.updated_at = this.formatDateTime(_modelInstance.updated_at);
    // 	});
    // 	this.before('update', async (_modelInstance) => {
    // 		_modelInstance.created_at = this.formatDateTime(_modelInstance.created_at);
    // 		_modelInstance.updated_at = this.formatDateTime(_modelInstance.updated_at);
    // 	});
    // }

    private static formatDateTime(datetime: any) {
        let value = new Date(datetime);
        return datetime
            ? value.getFullYear() +
                  '-' +
                  (value.getMonth() + 1) +
                  '-' +
                  value.getDate() +
                  ' ' +
                  value.getHours() +
                  ':' +
                  value.getMinutes() +
                  ':' +
                  value.getSeconds()
            : datetime;
    }

    @manyToMany(() => User, {
        pivotForeignKey: 'role_id',
        pivotRelatedForeignKey: 'account_id',
        pivotTable: 'link_accounts_roles',
    })
    public users: ManyToMany<typeof User>;

    @manyToMany(() => Permission, {
        pivotForeignKey: 'role_id',
        pivotRelatedForeignKey: 'permission_id',
        pivotTable: 'role_has_permissions',
    })
    public permissions: ManyToMany<typeof Permission>;
}
