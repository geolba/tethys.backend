import {
    column,
    SnakeCaseNamingStrategy,
    manyToMany,
    belongsTo,
    hasMany,
    computed,
    hasOne
} from '@adonisjs/lucid/orm';
import { DateTime } from 'luxon';
import dayjs from 'dayjs';
import Person from './person.js';
import User from './user.js';
import Title from './title.js';
import Description from './description.js';
import License from './license.js';
import Subject from './subject.js';
import File from './file.js';
import Coverage from './coverage.js';
import DatasetReference from './dataset_reference.js';
import Collection from './collection.js';
import DatasetIdentifier from './dataset_identifier.js';
import Project from './project.js';
import DocumentXmlCache from './DocumentXmlCache.js';
import DatasetExtension from '#models/traits/dataset_extension';
import type { ManyToMany } from "@adonisjs/lucid/types/relations";
import type { BelongsTo } from "@adonisjs/lucid/types/relations";
import type { HasMany } from "@adonisjs/lucid/types/relations";
import type { HasOne } from "@adonisjs/lucid/types/relations";

export default class Dataset extends DatasetExtension {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'documents';
    public static selfAssignPrimaryKey = false;

    @column({ isPrimary: true })
    public id: number;

    @column({})
    public server_state: string;

    @column({})
    public publisher_name: string;

    @column({ columnName: 'creating_corporation' })
    public creating_corporation: string;

    @column.dateTime({ 
        columnName: 'embargo_date',
        serialize: (value: Date | null) => {
            return value ? dayjs(value).format('YYYY-MM-DD') : value;
        },
    })
    public embargo_date: DateTime;

    @column({})
    public type: string;

    @column({})
    public language: string;

    @column({columnName: 'publish_id'})
    public publish_id: number | null = null;

    @column({})
    public project_id: number | null = null;

    @column({})
    public account_id: number | null = null;

    @column({})
    public editor_id: number | null = null;

    @column({})
    public reviewer_id: number | null = null;

    @column({})
    public reject_editor_note: string | null;

    @column({})
    public preferred_reviewer: string | null;

    @column({})
    public preferred_reviewer_email: string | null;

    @column({})
    public reject_reviewer_note: string | null;

    @column.dateTime({ columnName: 'server_date_published' })
    public server_date_published: DateTime;

    // @column.dateTime({ autoCreate: true, columnName: 'created_at' })
    @column.dateTime({
        serialize: (value: Date | null) => {
            return value ? dayjs(value).format('MMMM D YYYY HH:mm a') : value;
        },
        autoCreate: true,
        columnName: 'created_at',
    })
    public created_at: DateTime;

    @column.dateTime({
        serialize: (value: Date | null) => {
            return value ? dayjs(value).format('MMMM D YYYY HH:mm a') : value;
        },
        autoCreate: true,
        autoUpdate: true,
        columnName: 'server_date_modified',
    })
    public server_date_modified: DateTime;

    @manyToMany(() => Person, {
        pivotForeignKey: 'document_id',
        pivotRelatedForeignKey: 'person_id',
        pivotTable: 'link_documents_persons',
        pivotColumns: ['role', 'sort_order', 'allow_email_contact'],
    })
    public persons: ManyToMany<typeof Person>;

    /**
     * Get the account that the dataset belongs to
     */
    @belongsTo(() => User, {
        foreignKey: 'account_id',
    })
    public user: BelongsTo<typeof User>;

    @belongsTo(() => Project, {
        foreignKey: 'project_id',
    })
    public project: BelongsTo<typeof Project>;

    @hasMany(() => Title, {
        foreignKey: 'document_id',
    })
    public titles: HasMany<typeof Title>;

    @hasMany(() => Description, {
        foreignKey: 'document_id',
    })
    public descriptions: HasMany<typeof Description>;

    @manyToMany(() => License, {
        pivotForeignKey: 'document_id',
        pivotRelatedForeignKey: 'licence_id',
        pivotTable: 'link_documents_licences',
    })
    public licenses: ManyToMany<typeof License>;

    @manyToMany(() => Subject, {
        pivotForeignKey: 'document_id',
        pivotRelatedForeignKey: 'subject_id',
        pivotTable: 'link_dataset_subjects',
    })
    public subjects: ManyToMany<typeof Subject>;

    @hasMany(() => File, {
        foreignKey: 'document_id',
    })
    public files: HasMany<typeof File>;

    @hasOne(() => Coverage, {
        foreignKey: 'dataset_id',
    })
    public coverage: HasOne<typeof Coverage>;

    @hasMany(() => DatasetReference, {
        foreignKey: 'document_id',
    })
    public references: HasMany<typeof DatasetReference>;

    // Dataset.hasMany(Reference, {
    //     foreignKey: "related_document_id",
    //     as: "referenced_by",
    // });
    @hasMany(() => DatasetReference, {
        foreignKey: 'related_document_id',
    })
    public referenced_by: HasMany<typeof DatasetReference>;

    @manyToMany(() => Collection, {
        pivotForeignKey: 'document_id',
        pivotRelatedForeignKey: 'collection_id',
        pivotTable: 'link_documents_collections',
    })
    public collections: ManyToMany<typeof Collection>;

    @hasOne(() => DatasetIdentifier, {
        foreignKey: 'dataset_id',
    })
    public identifier: HasOne<typeof DatasetIdentifier>;

    @computed({
        serializeAs: 'main_title',
    })
    public get mainTitle() {
        // return `${this.firstName} ${this.lastName}`;
        const mainTitle = this.titles?.find((title) => title.type === 'Main');
        return mainTitle ? mainTitle.value : null;
    }

    @computed({
        serializeAs: 'main_abstract',
    })
    public get mainAbstract() {
        // return `${this.firstName} ${this.lastName}`;
        const mainTitle = this.descriptions?.find((desc) => desc.type === 'Abstract');
        return mainTitle ? mainTitle.value : null;
    }

    @manyToMany(() => Person, {
        pivotForeignKey: 'document_id',
        pivotRelatedForeignKey: 'person_id',
        pivotTable: 'link_documents_persons',
        pivotColumns: ['role', 'sort_order', 'allow_email_contact'],
        onQuery(query) {
            query.wherePivot('role', 'author');
        },
    })
    public authors: ManyToMany<typeof Person>;

    @manyToMany(() => Person, {
        pivotForeignKey: 'document_id',
        pivotRelatedForeignKey: 'person_id',
        pivotTable: 'link_documents_persons',
        pivotColumns: ['role', 'sort_order', 'allow_email_contact', 'contributor_type'],
        onQuery(query) {
            query.wherePivot('role', 'contributor');
        },
    })
    public contributors: ManyToMany<typeof Person>;

    @hasOne(() => DocumentXmlCache, {
        foreignKey: 'document_id',
    })
    public xmlCache: HasOne<typeof DocumentXmlCache>;

    /**
     * Get the account that the dataset belongs to
     */
    @belongsTo(() => User, {
        foreignKey: 'editor_id',
    })
    public editor: BelongsTo<typeof User>;

    @belongsTo(() => User, {
        foreignKey: 'reviewer_id',
    })
    public reviewer: BelongsTo<typeof User>;

    static async earliestPublicationDate(): Promise<Dataset | null> {
        const serverState = 'published';

        const model = await this.query().where('server_state', serverState).orderBy('server_date_published', 'asc').first();

        return model || null;
    }

    static async getMax (column: string) {
        let dataset = await this.query().max(column + ' as max_publish_id').firstOrFail();
        return dataset.$extras.max_publish_id;
      }

    @computed({
        serializeAs: 'remaining_time',
    })
    public get remainingTime() {
        const dateFuture = this.server_date_modified.plus({ days: 14 });
        if (this.server_state === 'approved') {
            const now = DateTime.now();
            let duration = dateFuture.diff(now, ['days', 'hours', 'months']).toObject();
            return duration.days;
        } else {
            return 0;
        }
    }
}
