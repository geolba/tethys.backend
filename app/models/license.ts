import { column, SnakeCaseNamingStrategy } from '@adonisjs/lucid/orm';
import BaseModel from './base_model.js';

export default class License extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'document_licences';
    public static selfAssignPrimaryKey = false;
    public static timestamps = false;
    public static fillable: string[] = [
        'name_long',
        'name',
        'language',
        'link_licence',
        'link_logo',
        'desc_text',
        'desc_markup',
        'comment_internal',
        'mime_type',
        'sort_order',
        'language',
        'active',
        'pod_allowed',
    ];

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public active: boolean;

    @column({})
    public langauge: string;

    @column({})
    public link_licence: string;

    @column({})
    public link_logo: string;

    @column({})
    public display_name: string;

    @column({})
    public name_long: string;

    @column({})
    public name: string;

    @column({})
    public sortOrder: number;
}
