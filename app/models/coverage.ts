import { column, SnakeCaseNamingStrategy, belongsTo } from '@adonisjs/lucid/orm';
import { DateTime } from 'luxon';
import Dataset from './dataset.js';
import BaseModel from './base_model.js';
import type { BelongsTo } from "@adonisjs/lucid/types/relations";

export default class Coverage extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static primaryKey = 'id';
    public static table = 'coverage';
    public static selfAssignPrimaryKey = false;
    public static fillable: string[] = [
        'elevation_min',
        'elevation_max',
        'elevation_absolut',
        'depth_min',
        'depth_max',
        'depth_absolut',
        'time_min',
        'time_max',
        'time_absolut',
        'x_min',
        'x_max',
        'y_min',
        'y_max',
    ];

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public dataset_id: number;

    @column({})
    public elevation_min: number;

    @column({})
    public elevation_max: number;

    @column({})
    public elevation_absolut: number;

    @column({})
    public depth_min: number;

    @column({})
    public depth_max: number;

    @column({})
    public depth_absolut: number;

    @column.dateTime({})
    public time_min: DateTime;

    @column.dateTime({})
    public time_max: DateTime;

    @column.dateTime({})
    public time_absolut: DateTime;

    @column({})
    public x_min: number;

    @column({})
    public x_max: number;

    @column({})
    public y_min: number;

    @column({})
    public y_max: number;

    @column.dateTime({
        autoCreate: true,
    })
    public created_at: DateTime;

    @column.dateTime({
        autoCreate: true,
        autoUpdate: true,
    })
    public updated_at: DateTime;

    @belongsTo(() => Dataset, {
        foreignKey: 'dataset_id',
    })
    public dataset: BelongsTo<typeof Dataset>;
}
