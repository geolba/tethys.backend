import { column, SnakeCaseNamingStrategy, manyToMany, computed} from '@adonisjs/lucid/orm';
import BaseModel from './base_model.js';

import { DateTime } from 'luxon';
import dayjs from 'dayjs';
import Dataset from './dataset.js';
import type { ManyToMany } from "@adonisjs/lucid/types/relations";

export default class Subject extends BaseModel {
    public static namingStrategy = new SnakeCaseNamingStrategy();
    public static table = 'dataset_subjects';
    public static selfAssignPrimaryKey = false;

    @column({
        isPrimary: true,
    })
    public id: number;

    @column({})
    public language: string;

    @column({})
    public type: string;

    @column({})
    public value: string;

    @column({})
    public external_key: string;

    @column.dateTime({
        serialize: (value: Date | null) => {
            return value ? dayjs(value).format('MMMM D YYYY HH:mm a') : value;
        },
        autoCreate: true,
    })
    public created_at: DateTime;

    @column.dateTime({
        serialize: (value: Date | null) => {
            return value ? dayjs(value).format('MMMM D YYYY HH:mm a') : value;
        },
        autoCreate: true,
        autoUpdate: true,
    })
    public updated_at: DateTime;

    // @beforeCreate()
    // @beforeUpdate()
    // public static async resetDate(role) {
    //     role.created_at = this.formatDateTime(role.created_at);
    //     role.updated_at = this.formatDateTime(role.updated_at);
    // }

    // private static formatDateTime(datetime) {
    //     let value = new Date(datetime);
    //     return datetime
    //         ? value.getFullYear() +
    //               '-' +
    //               (value.getMonth() + 1) +
    //               '-' +
    //               value.getDate() +
    //               ' ' +
    //               value.getHours() +
    //               ':' +
    //               value.getMinutes() +
    //               ':' +
    //               value.getSeconds()
    //         : datetime;
    // }
    @computed()
    public get dataset_count() : number{
        const count = this.$extras.datasets_count; //my pivot column name was "stock"
        return count;
    }

    @manyToMany(() => Dataset, {
        pivotForeignKey: 'subject_id',
        pivotRelatedForeignKey: 'document_id',
        pivotTable: 'link_dataset_subjects',
    })
    public datasets: ManyToMany<typeof Dataset>;
}
