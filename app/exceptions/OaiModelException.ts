import { StatusCodes } from 'http-status-codes';
// import HTTPException from './HttpException';
import { OaiErrorCodes } from './OaiErrorCodes.js';

export class ErrorCode {
    public static readonly Unauthenticated = 'Unauthenticated';
    public static readonly NotFound = 'NotFound';
    public static readonly MaximumAllowedGrade = 'MaximumAllowedGrade';
    public static readonly AsyncError = 'AsyncError';
    public static readonly UnknownError = 'UnknownError';
}

export class ErrorModel {
    /**
     * Unique error code which identifies the error.
     */
    public code: string;
    /**
     * Status code of the error.
     */
    public status: number;
    /**
     * Any additional data that is required for translation.
     */
    // public metaData?: any;
}

export class OaiModelException extends Error {
    public status: number;
    public message: string;
    public oaiCode: number;

    // constructor(status: number, message: string) {
    //   super(message);
    //   this.status = status;
    //   this.message = message;
    // }
    constructor(status: number, message: string, oaiCode: number) {
        super(message);
        this.status = status;
        this.message = message;
        this.oaiCode = oaiCode;
    }
    // constructor(code: string = ErrorCode.UnknownError, message: any = null) {
    //     super(code);
    //     Object.setPrototypeOf(this, new.target.prototype);
    //     this.name = code;
    //     this.status = 500;
    //     this.message = message;
    //     // switch (code) {
    //     //   case ErrorCode.Unauthenticated:
    //     //     this.status = 401;
    //     //     break;
    //     //   case ErrorCode.MaximumAllowedGrade:
    //     //     this.status = 400;
    //     //     break;
    //     //   case ErrorCode.AsyncError:
    //     //     this.status = 400;
    //     //     break;
    //     //   case ErrorCode.NotFound:
    //     //     this.status = 404;
    //     //     break;
    //     //   default:
    //     //     this.status = 500;
    //     //     break;
    //     // }
    //   }
}

export class BadOaiModelException extends OaiModelException {
    constructor(message?: string) {
        super(StatusCodes.INTERNAL_SERVER_ERROR, message || 'bad Request', OaiErrorCodes.BADARGUMENT);
        this.stack = '';
    }
}

// export default OaiModelexception;
