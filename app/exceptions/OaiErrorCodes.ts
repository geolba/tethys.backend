export enum OaiErrorCodes {
    BADVERB = 1010,
    BADARGUMENT = 1011,
    CANNOTDISSEMINATEFORMAT = 1012,
    BADRESUMPTIONTOKEN = 1013,
    NORECORDSMATCH = 1014,
    IDDOESNOTEXIST = 1015,
}

// 👇️ default export
// export { OaiErrorCodes };

// https://medium.com/@juliapassynkova/map-your-typescript-enums-e402d406b229
export const OaiModelError = new Map<number, string>([
    [OaiErrorCodes.BADVERB, 'badVerb'],
    [OaiErrorCodes.BADARGUMENT, 'badArgument'],
    [OaiErrorCodes.NORECORDSMATCH, 'noRecordsMatch'],
    [OaiErrorCodes.CANNOTDISSEMINATEFORMAT, 'cannotDisseminateFormat'],
    [OaiErrorCodes.BADRESUMPTIONTOKEN, 'badResumptionToken'],
    [OaiErrorCodes.IDDOESNOTEXIST, 'idDoesNotExist'],
]);

// class OaiModelError {
//     // const BADVERB = 1010;
//     // const BADARGUMENT = 1011;
//     // const CANNOTDISSEMINATEFORMAT = 1012;
//     // const BADRESUMPTIONTOKEN = 1013;
//     // const NORECORDSMATCH = 1014;
//     // const IDDOESNOTEXIST = 1015;

//     protected static $oaiErrorCodes = {
//         OaiErrorCodes. 'badVerb',
//         BADARGUMENT : 'badArgument',
//         NORECORDSMATCH: 'noRecordsMatch',
//         CANNOTDISSEMINATEFORMAT: 'cannotDisseminateFormat',
//        BADRESUMPTIONTOKEN: 'badResumptionToken',
//        IDDOESNOTEXIST: 'idDoesNotExist',
//     };

// public static function mapCode($code)
// {
//     if (false === array_key_exists($code, self::$oaiErrorCodes)) {
//         throw new OaiModelException("Unknown oai error code $code");
//     }
//     return self::$oaiErrorCodes[$code];
// }

// }
