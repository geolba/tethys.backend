# Tethys Research Repository Backend System

Welcome to the Tethys Research Repository Backend System! This is the backend component responsible for managing datasets, users, and the core functionality of the Tethys Data Research Repository.

## Table of Contents

-   [Getting Started](#getting-started)
    -   [Prerequisites](#prerequisites)
    -   [Installation](#installation)
-   [Usage](#usage)
-   [Configuration](#configuration)
-   [Database](#database)
-   [API Documentation](#api-documentation)
-   [Contributing](#contributing)
-   [License](#license)

## Getting Started

### Prerequisites

Before you begin, ensure you have met the following requirements:

-   Node.js and npm installed on your development machine.
-   A running PostgreSQL database instance.
-   AdonisJS CLI globally installed.

### Installation

1. Clone this repository:

    ```bash
    git clone https://gitea.geologie.ac.at/geolba/tethys.backend.git
    ```
