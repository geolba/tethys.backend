const styles = {
    basic: {
        aside: 'bg-gray-800',
        asideScrollbars: 'aside-scrollbars-gray',
        asideBrand: 'bg-gray-900 text-white',
        asideMenuItem: 'text-gray-300 hover:text-white',
        asideMenuItemActive: 'font-bold text-cyan-300',
        asideMenuDropdown: 'bg-gray-700/50',
        navBarItemLabel: 'text-black',
        // navBarItemLabelHover: 'hover:text-blue-500',
        navBarItemLabelHover: 'hover:text-lime-dark',
        // navBarItemLabelActiveColor: 'text-blue-600',
        navBarItemLabelActiveColor: 'text-lime-dark',
        overlay: 'from-gray-700 via-gray-900 to-gray-700',
    },

    white: {
        aside: 'bg-white',
        asideScrollbars: 'aside-scrollbars-light',
        asideBrand: '',
        asideMenuItem: 'text-blue-600 hover:text-black dark:text-white',
        asideMenuItemActive: 'font-bold text-black dark:text-white',
        asideMenuDropdown: 'bg-gray-100/75',
        navBarItemLabel: 'text-blue-600',
        navBarItemLabelHover: 'hover:text-black',
        navBarItemLabelActiveColor: 'text-black',
        overlay: 'from-white via-gray-100 to-white',
    },
};
export default styles;