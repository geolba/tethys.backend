import { defineStore } from 'pinia';
// import axios from 'axios';
// import dayjs from 'dayjs';
import type { Map as LeafletMap } from 'leaflet' ;

export const TimeService = defineStore('map', {
    state: () => ({
        // dataset: {} as Dataset,
        mapService: new Map<string, any>(),
    }),
    actions: {
        getMap(id: string) {
            return this.mapService.get(id);
        },

        setMap(id: string, map: LeafletMap) {
            this.mapService.set(id, map);
        },

        hasMap(id: string): boolean {
            return this.mapService.has(id);
        },

        deleteMap(id: string): boolean {
            return this.mapService.delete(id);
        },

        // getDuration(timespan: Timespan): dayjs. {
        //     const from = dayjs(timespan.from);
        //     const to = dayjs(timespan.to);
        //     return dayjs.duration(to.diff(from));
        // },
    },
});
