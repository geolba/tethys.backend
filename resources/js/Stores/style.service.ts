import { defineStore } from 'pinia';
import styles from '@/styles';
import { darkModeKey, styleKey } from '@/config';


interface StyleState {
    [key: string]: string | boolean;
    asideStyle: string;
    asideScrollbarsStyle: string;
    asideBrandStyle: string;
    asideMenuItemStyle: string;
    asideMenuItemActiveStyle: string;
    asideMenuDropdownStyle: string;
    navBarItemLabelStyle: string;
    navBarItemLabelHoverStyle: string;
    navBarItemLabelActiveColorStyle: string;
    overlayStyle: string;
    darkMode: boolean;
}

// Define StyleService store
export const StyleService = defineStore('style', {
    state: (): StyleState => ({
        // Styles
        asideStyle: '',
        asideScrollbarsStyle: '',
        asideBrandStyle: '',
        asideMenuItemStyle: '',
        asideMenuItemActiveStyle: '',
        asideMenuDropdownStyle: '',
        navBarItemLabelStyle: '',
        navBarItemLabelHoverStyle: '',
        navBarItemLabelActiveColorStyle: '',
        overlayStyle: '',
        // Dark mode default false
        darkMode: false,
    }),
    actions: {
        // Set style based on payload value ('basic' or 'white')
        setStyle(payload: 'basic' | 'white') {
            if (!styles[payload]) {
                return;
            }
            if (typeof localStorage !== 'undefined') {
                localStorage.setItem(styleKey, payload);
            }
            const style = styles[payload] as Record<string, string>;
            for (const key in style) {
                // let keyStyle: string  = `${key}Style`;//key as keyof typeof style;
                this[`${key}Style` as keyof StyleState] = style[key];
            }
        },
        // Toggle dark mode
        setDarkMode(payload?: boolean) {
            this.darkMode = payload !== undefined ? payload : !this.darkMode;
            if (typeof localStorage !== 'undefined') {
                localStorage.setItem(darkModeKey, this.darkMode ? '1' : '0');
            }
            if (typeof document !== 'undefined') {
                document.body.classList[this.darkMode ? 'add' : 'remove']('dark-scrollbars');
                document.documentElement.classList[this.darkMode ? 'add' : 'remove']('dark-scrollbars-compat');
            }
        },

         // Toggle dark mode
         setLocale(payload?: boolean) {
            this.darkMode = payload !== undefined ? payload : !this.darkMode;
            if (typeof localStorage !== 'undefined') {
                localStorage.setItem(darkModeKey, this.darkMode ? '1' : '0');
            }
            if (typeof document !== 'undefined') {
                document.body.classList[this.darkMode ? 'add' : 'remove']('dark-scrollbars');
                document.documentElement.classList[this.darkMode ? 'add' : 'remove']('dark-scrollbars-compat');
            }
        },
    },
});