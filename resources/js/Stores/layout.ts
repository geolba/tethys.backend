import { defineStore } from 'pinia';

export const LayoutService = defineStore('layout', {
    state: () => ({
        isAsideMobileExpanded: false, // via action
        isAsideLgActive: false,
    }),

    actions: {
        asideMobileToggle() {
            this.isAsideMobileExpanded = !this.isAsideMobileExpanded;
        },
    },
});
