import { defineStore } from 'pinia';
import axios from 'axios';
import { Dataset } from '@/Dataset';
import menu from '@/menu';

export interface Person {
    id: number;
    name: string;
    email: string;
    name_type: string;
    identifier_orcid: string;
    datasetCount: string;
    created_at: string;
}

interface TransactionItem {
    amount: number;
    account: string;
    name: string;
    date: string;
    type: string;
    business: string;
}

export enum State {
    STATE_DISABLED = 0,
    STATE_CREATED = 1,
    STATE_ENABLED = 2,
}

export const saveState = async (data: any) => {
    const url = '/api/twofactor_totp/settings/enable';

    const resp = await axios.post(url, data);
    return resp.data;
};

export const generateCodes = async () => {
    const url = '/api/twofactor_backupcodes/settings/create';

    // return Axios.post(url, {}).then((resp) => resp.data);
    const resp = await axios.post(url, {});
    return resp.data;
};

// Anfrage staet : 1

// ANtwort json:
// state: 1
// secret:"OX7IQ4OI3GXGFPHY"
// qUrl:"https://odysseus.geologie.ac.at/apps/twofactor_totp/settings/enable"

export const MainService = defineStore('main', {
    state: () => ({
        /* User */
        userName: '',
        userEmail: null,
        userAvatar: null,

        /* Field focus with ctrl+k (to register only once) */
        isFieldFocusRegistered: false,

        /* Sample data for starting dashboard(commonly used) */
        clients: [],
        history: [] as Array<TransactionItem>,

        // api based data
        authors: [] as Array<Person>,
        // persons: [] as Array<Person>,
        datasets: [],
        files: [],

        dataset: {} as Dataset,

        menu: menu,

        totpState: 0,

        backupcodesEnabled: false,
        total: 0,
        used: 0,
        codes: [],

        graphData: {},
    }),
    actions: {
        // payload = authenticated user
        setUser(payload: any) {
            if (payload.name) {
                this.userName = payload.name;
            }
            if (payload.email) {
                this.userEmail = payload.email;
            }
            if (payload.avatar) {
                this.userAvatar = payload.avatar;
            }
        },

        setDataset(payload: any) {
            this.dataset = payload;

            // this.dataset = {
            //     language: language,
            //     licenses: payload.licenses,
            //     rights: payload.rights,
            //     type: payload.type,
            //     creating_corporation: payload.creating_corporation,
            //     titles: payload.titles,
            //     descriptions: payload.descriptions,
            //     authors: payload.authors,
            //     project_id: payload.project_id,
            //     embargo_date: payload.embargo_date,
            // } as Dataset;
            // for (let index in payload.titles) {
            //     let title = payload.titles[index];
            //     title.language = language;
            // }
        },

        clearDataset() {
            this.dataset = null;
        },

        fetch(sampleDataKey: string) {
            // sampleDataKey= clients or history
            axios
                .get(`/data-sources/${sampleDataKey}.json`)
                .then((r) => {
                    if (r.data && r.data.data) {
                        this[sampleDataKey] = r.data.data;
                    }
                })
                .catch((error) => {
                    alert(error.message);
                });
        },

        fetchApi(sampleDataKey: string) {
            // sampleDataKey= authors or datasets
            axios
                .get(`/api/${sampleDataKey}`)
                .then((r) => {
                    if (r.data) {
                        this[sampleDataKey] = r.data;
                    }
                })
                .catch((error) => {
                    alert(error.message);
                });
        },

        setState(state: any) {
            this.totpState = state;
        },

        async create(): Promise<{ url: any; secret: any; svg: any }> {
            const { state, secret, url, svg } = await saveState({ state: State.STATE_CREATED });
            this.totpState = state;
            return { url, secret, svg };
            // .then(({ state, secret, qrUrl }) => {
            //     this.totpState = state;
            //     return { qrUrl, secret };
            // })
            // .catch((error) => {
            //     alert(error.message);
            // });
        },

        async disable() {
            const { state, backupState } = await saveState({ state: State.STATE_DISABLED });
            this.totpState = state;

            this.backupcodesEnabled = backupState.enabled;
            this.total = backupState.total;
            this.used = backupState.used;
        },

        async confirm(code: string) {
            const { state } = await saveState({
                state: State.STATE_ENABLED,
                code,
            });
            this.totpState = state;
        },

        async fetchChartData(year: string) {
            // sampleDataKey= authors or datasets
            axios
                .get(`/api/statistic/${year}`)
                .then((r) => {
                    if (r.data) {
                        this.graphData = r.data;
                    }
                })
                .catch((error) => {
                    alert(error.message);
                });
        },

        async generate(): Promise<void> {
            const { codes, backupState } = await generateCodes();
            // this.totpState = state;
            this.codes = codes;
            this.backupcodesEnabled = backupState.enabled;
            this.total = backupState.total;
            this.used = backupState.used;
        },

        // fetchfiles(id) {
        //     // sampleDataKey= authors or datasets
        //     axios
        //         .get(`api/files/${id}`)
        //         .then((r) => {
        //             if (r.data) {
        //                 this[sampleDataKey] = r.data;
        //             }
        //         })
        //         .catch((error) => {
        //             alert(error.message);
        //         });
        // },
    },
});
