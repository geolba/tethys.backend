// src/stores/locale.ts
import { defineStore } from 'pinia';
import dayjs from '@/utils/dayjs';
import { getBrowserLocale } from '@/utils/tethyscloud-l10n';

export const LocaleStore = defineStore('locale', {  

    state: () => ({
        locale: 'en',
    }),

    actions: {
        setLocale(locale: string) {
            if (typeof localStorage !== 'undefined') {
                localStorage.setItem('app-locale', locale);
            }
            this.locale = locale;
            dayjs.locale(locale); // Update dayjs locale
        },

        initializeLocale() {
            let locale = 'en';

            // Check if localStorage is available and has a saved locale
            if (typeof localStorage !== 'undefined') {
                const savedLocale = localStorage.getItem('app-locale');
                if (savedLocale) {
                    locale = savedLocale;
                } else {
                    // Get locale from the browser if not saved in localStorage
                    locale = getBrowserLocale({ languageCodeOnly: true });
                }
            } else {
                // Fallback to the browser locale if localStorage is not available
                locale = getBrowserLocale({ languageCodeOnly: true });
            }

            this.setLocale(locale);
        },
    },
});
