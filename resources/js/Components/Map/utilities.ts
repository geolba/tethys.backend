// extend an object with properties of one or more other objects
export function extend(dest) {
    let i;
    let j;
    let len;

    for (j = 1, len = arguments.length; j < len; j++) {
        let src = arguments[j];
        for (i in src) {
            dest[i] = src[i];
        }
    }
    return dest;
}
