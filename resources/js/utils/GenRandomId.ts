const GenRandomId = (length: number) => {
	return Math.random()
		.toString(36)
		.replace(/[^a-z]+/g, '')
		.slice(0, length || 5)
}

export default GenRandomId