// src/plugins/dayjs.ts
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import 'dayjs/locale/de';
import 'dayjs/locale/en';

const extendedDayjs = dayjs.extend(relativeTime);

export const setDayjsLocale = (locale: string) => {
  extendedDayjs.locale(locale);
};

// // Set a default locale initially
// setDayjsLocale('en');

export default extendedDayjs;