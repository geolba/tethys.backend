export function loadState<T>(app: string, key: string, fallback?: T): T {
    const elem = <HTMLInputElement>document.querySelector(`#initial-state-${app}-${key}`);
    if (elem === null) {
        if (fallback !== undefined) {
            return fallback;
        }
        throw new Error(`Could not find initial state ${key} of ${app}`);
    }

    try {
        const value = atob(elem.value);
        return JSON.parse(value);
    } catch (e) {
        throw new Error(`Could not parse initial state ${key} of ${app}`);
    }
}
