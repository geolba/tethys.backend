import {
    // mdiMonitor,
    mdiGithub,
    mdiAccountEye,
    mdiAccountGroup,
    mdiAccountEdit,
    mdiPublish,
    mdiAccountArrowUp,
    mdiFormatListNumbered,
    mdiLock,
    mdiFormatListGroup,
    mdiShieldCrownOutline,
    mdiLicense,
    mdiFileDocument,
} from '@mdi/js';

export default [
    {
        // route: 'dataset.create',
        icon: mdiAccountEdit ,
        label: 'Personal',
        // roles: ['submitter'],
        isOpen: true,
        children: [
            {
                route: 'settings.user',
                icon: mdiLock,
                label: 'Security',
            },
            // {
            //     route: 'dataset.create',
            //     icon: mdiPublish,
            //     label: 'Create Dataset',
            // },
        ],
    },
    // {
    //     route: 'apps.dashboard',
    //     icon: mdiMonitor,
    //     label: 'Dashboard',
    // },
    // {
    //   route: 'permission.index',
    //   icon: mdiAccountKey,
    //   label: 'Permissions'
    // },
    // {
    //   route: 'role.index',
    //   icon: mdiAccountEye,
    //   label: 'Roles'
    // },
    {
        icon: mdiShieldCrownOutline,
        label: 'Administration',
        roles: ['administrator'],
        isOpen: true,
        permanent: true,
        children: [
            {
                route: 'settings.overview',
                icon: mdiFormatListGroup,
                label: 'Overview',
                roles: ['administrator'],
            },
            {
                route: 'settings.user.index',
                icon: mdiAccountGroup,
                label: 'Users',
                roles: ['administrator'],
            },
            {
                route: 'settings.role.index',
                icon: mdiAccountEye,
                label: 'Roles',
                roles: ['administrator'],
            },
            {
                route: 'settings.mimetype.index',
                icon: mdiFileDocument,
                label: 'Mime Types',
                roles: ['administrator'],
            },
            {
                route: 'settings.license.index',
                icon: mdiLicense,
                label: 'Licenses',
                roles: ['administrator'],
            },
        ],
    },

    {
        // route: 'dataset.create',
        icon: mdiAccountArrowUp,
        label: 'Submitter',
        roles: ['submitter'],
        isOpen: false,
        children: [
            {
                route: 'dataset.list',
                icon: mdiFormatListNumbered,
                label: 'All my datasets',
            },
            {
                route: 'dataset.create',
                icon: mdiPublish,
                label: 'Create Dataset',
            },
        ],
    },
    {
        // route: 'dataset.create',
        icon: mdiAccountEdit,
        label: 'Editor',
        roles: ['editor'],
        isOpen: false,
        children: [
            {
                route: 'editor.dataset.list',
                icon: mdiFormatListNumbered,
                label: 'All my datasets',
            },
            // {
            //     route: 'dataset.create',
            //     icon: mdiPublish,
            //     label: 'Create Dataset',
            // },
        ],
    },
    {
        // route: 'dataset.create',
        icon: mdiAccountEdit,
        label: 'Reviewer',
        roles: ['reviewer'],
        isOpen: false,
        children: [
            {
                route: 'reviewer.dataset.list',
                icon: mdiFormatListNumbered,
                label: 'All my datasets',
            },
            // {
            //     route: 'dataset.create',
            //     icon: mdiPublish,
            //     label: 'Create Dataset',
            // },
        ],
    },
    // {
    //     route: 'dataset.create',
    //     icon: mdiDatabasePlus,
    //     label: 'Create Dataset',
    // },
    {
        href: 'https://gitea.geologie.ac.at/geolba/tethys',
        icon: mdiGithub,
        label: 'Gitea',
        target: '_blank',
    },
    {
        href: '/oai',
        icon: mdiAccountEye,
        label: 'OAI',
        target: '_blank',
    },
];
