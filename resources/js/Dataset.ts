import { Ref } from 'vue';
import { DateTime } from 'luxon';

export interface User {
    id: number;
    login: string;
    email: string;
    password: string;
    createdAt: DateTime;
    updatedAt: DateTime;
    roles: Array<Role>;
}

export interface Role {
    id: number;
    display_name: string;
    name: string;
    description: string;
    created_at: DateTime;
    updated_at: DateTime;
}
export interface License {
    id: any;
    // Add other properties as needed
}
export interface Dataset {
    [key: string]:
        | string
        | Ref<string>
        | boolean
        | Array<Title>
        | Array<Description>
        | Array<Person>
        | number
        | (IErrorMessage | undefined)
        | Coverage
        | Array<DatasetReference>
        | Array<File>
        | (Array<number> | Array<License>)
        |  Array<TethysFile>;
    language: string;
    licenses: Array<number> | Array<License>;
    rights: boolean;
    type: string;
    creating_corporation: string;
    titles: Array<Title>;
    descriptions: Array<Description>;
    authors: Array<Person>;
    contributors: Array<Person>;
    project_id?: number;
    reviewer_id?: number;
    embargo_date?: string;
    coverage: Coverage;
    errors?: IErrorMessage;
    // async (user): Promise<void>;
    subjects: Array<Subject>;
    references: Array<DatasetReference>;
    files: Array<TethysFile>;
    filesToDelete?: Array<TethysFile>;
    // upload: TethysFile
}

/** Provides information about files and allows JavaScript in a web page to access their content. */
// export interface TethysFile {

//     readonly lastModified: number;
//     readonly name: string;
//     readonly webkitRelativePath: string;
//     id: number;
//     label: string;
//     sorting: number;
//     filePath: string;
//     fileSrc: string;
// }

export interface TethysFile {
    readonly lastModified: number;
    readonly name: string;
    readonly webkitRelativePath: string;
    id?: number;
    label: string;
    // sorting: number;
    // path_name?: string; //only db path_name
    filePath?: string;
    fileSrc?: string;
    blob: Blob;
    fileData?: any;

    //additional:
    comment?: string;
    document_id?: number;
    file_size: number;
    language?: string;
    mime_type: string;
    type?: string;

    size: number;
    sort_order: number;
    visible_in_frontdoor: boolean;
    visible_in_oai: boolean;
}

export interface Subject {
    id?: number;
    language: string;
    type: string;
    value: string;
    external_key?: string;
    dataset_count?: number;
}
export interface DatasetReference {
    // id: number;
    value: string;
    label: string;
    type: string;
    relation: string;
}

export interface Title {
    id?: number;
    value: string;
    type: string;
    language: string | Ref<string>;
}

export interface Description {
    id?: number;
    value: string;
    type: string;
    language: string | Ref<string>;
}

export interface Person {
    id?: number;
    name?: string;
    email: string;
    name_type?: string;
    identifier_orcid?: string;
    datasetCount?: string;
    created_at?: string;
    status: boolean;
}

interface IErrorMessage {
    [key: string]: Array<string>;
}

export interface Coverage {
    x_min?: number;
    y_min?: number;
    x_max?: number;
    y_max?: number;
    elevation_min?: number;
    elevation_max?: number;
    elevation_absolut?: number;

    depth_min?: number;
    depth_max?: number;
    depth_absolut?: number;

    time_min?: number;
    time_max?: number;
    time_absolut?: number;
}

export interface OpensearchDocument {
    abstract_additional: Array<string>;
    abstract_output: string;
    author: Array<string>;
    author_sort: Array<string>;
    belongs_to_bibliography: boolean;
    creating_corporation: string;
    doctype: string;
    geo_location: string;
    id: number;
    identifier: Identifier;
    language: string;
    licence: string;
    publisher_name: string;
    server_date_published: Array<number>;
    subject: Array<string>;
    title_output: string;
    year: number;
    year_inverted: number;
}
export interface Identifier {
    created_at: string;
    dataset_id: number;
    id: number;
    status: string; //'findable'
    type: string; //'doi'
    updated_at: string; //'2023-03-09T09:48:28.000Z'
    value: string; //'10.24341/tethys.209'
}

// export enum LoginState {
//     STATE_DISABLED = 0,
//     STATE_VALIDATED = 1,
//     STATE_2FA_AUTHENTICATED = 1,   
// }