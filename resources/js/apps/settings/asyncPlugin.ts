import { loadTranslations } from '@/utils/tethyscloud-l10n';

// export const register = async () => {
//     await loadTranslations('settings');
// };

// asyncPlugin.js
export default {
    async install(app: string) {
      // Perform your async setup logic here
      await loadTranslations(app);
    }
  };