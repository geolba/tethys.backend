import '../css/app.css';
import { createApp, h } from 'vue';
import { Inertia } from '@inertiajs/inertia';

import { createInertiaApp } from '@inertiajs/vue3';
// import DefaultLayout from '@/Layouts/Default.vue';
import { createPinia } from 'pinia';
import { StyleService } from '@/Stores/style.service';
import { LayoutService } from '@/Stores/layout';
import { LocaleStore } from '@/Stores/locale';
import { darkModeKey, styleKey } from '@/config';
// import type { DefineComponent } from 'vue';
// import { resolvePageComponent } from '@adonisjs/inertia/helpers';
const pinia = createPinia();
// import i18n from './i18n';
import { EmitterPlugin } from '@/EmitterDirective';

import { initRoutes } from '@eidellev/adonis-stardust/client/index.js';
initRoutes();

// import { loadTranslations } from './utils/tethyscloud-l10n';
import asyncPlugin from '@/apps/settings/asyncPlugin';
// const translation = await asyncPlugin.install('settings');

// interface SetupOptions {
//     el: Element;
//     App: App;
//     props: Record<string, any>;
//     plugin: Plugin;
// }

// import '@fontsource/archivo-black/index.css';
// import '@fontsource/inter/index.css';

createInertiaApp({
    progress: {
        // color: '#4B5563',
        color: '#22C55E',
    },
    // Webpack
    // resolve: (name) => require(`./Pages/${name}`),
    // resolve: (name) => require(`./Pages/${name}.vue`),
    // add default layout
    // resolve: (name) => {
    //     const page = require(`./Pages/${name}.vue`).default;
    //     // if (!page.layout) {
    //     // 	page.layout = DefaultLayout;
    //     // }
    //     return page;
    // },
    resolve: async (name: string) => {
        // Dynamically import the Vue component using import
        const { default: page } = await import(`./Pages/${name}.vue`);
        // const page = require(`./Pages/${name}.vue`).default;
        // if (!page.layout) {
        // 	page.layout = DefaultLayout;
        // }
        return page;
    },
    // resolve: (name) => {
    //     return resolvePageComponent(
    //       `./Pages/${name}.vue`,
    //       import.meta.glob<DefineComponent>('./pages/**/*.vue'),
    //     )
    //   },

    setup({ el, App, props, plugin }) {
        const app = createApp({ render: () => h(App, props) })
            .use(plugin)
            .use(pinia)
            // .use(i18n)
            .use(EmitterPlugin);
        // .component('inertia-link', Link)

        asyncPlugin.install('settings').then(() => {
            app.mount(el);
        });
    },
    
});

const styleService = StyleService(pinia);
const layoutService = LayoutService(pinia);
const localeService = LocaleStore(pinia);

localeService.initializeLocale();
// const mainService = MainService(pinia);
// mainService.setUser(user);

/* App style */
styleService.setStyle(localStorage[styleKey] ?? 'basic');

/* Dark mode */
if ((!localStorage[darkModeKey] && window.matchMedia('(prefers-color-scheme: dark)').matches) || localStorage[darkModeKey] === '1') {
    styleService.setDarkMode(true);
}

/* Collapse mobile aside menu on route change */
Inertia.on('navigate', () => {
    layoutService.isAsideMobileExpanded = false;
    layoutService.isAsideLgActive = false;
});
