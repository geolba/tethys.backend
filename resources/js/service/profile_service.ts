import axios from 'axios';
// import { getCurrentUser } from '@nextcloud/auth'
// import { generateOcsUrl } from '@nextcloud/router'
import { stardust } from '@eidellev/adonis-stardust/client';
// import { confirmPassword } from '@nextcloud/password-confirmation'
import '@nextcloud/password-confirmation/dist/style.css'

/**
 * Save the visibility of the profile parameter
 *
 * @param {string} paramId the profile parameter ID
 * @param {string} visibility the visibility
 * @return {object}
 */
// export const saveProfileParameterVisibility = async (paramId, visibility) => {
// 	const userId = getCurrentUser().uid
// 	const url = generateOcsUrl('/profile/{userId}', { userId })

// 	await confirmPassword()

// 	const res = await axios.put(url, {
// 		paramId,
// 		visibility,
// 	})

// 	return res.data
// }

/**
 * Save profile default
 *
 * @param {boolean} isEnabled the default
 * @return {object}
 */
export const saveProfileDefault = async (isEnabled: boolean) => {
	// Convert to string for compatibility
	let isEnabledString = isEnabled ? '1' : '0'

	// https://rhea.geosphere.at/ocs/v2.php/apps/provisioning_api/api/v1/config/apps/settings/profile_enabled_by_default
	// const url = generateOcsUrl('/apps/provisioning_api/api/v1/config/apps/{appId}/{key}', {
	// 	appId: 'settings',
	// 	key: 'profile_enabled_by_default',
	// })

	let appId= 'settings';
	let	key= 'profile_enabled_by_default';
	const url = stardust.route('settings.role.show', [appId, key]);
	
	// await confirmPassword()

	const res = await axios.post(url, {
		value: isEnabledString,
	})

	return res.data
}
