import { defineConfig } from '@adonisjs/inertia';
import type { HttpContext } from '@adonisjs/core/http';

export default defineConfig({
    /**
     * Path to the Edge view that will be used as the root view for Inertia responses
     */
    rootView: 'app',

    /**
     * Data that should be shared with all rendered pages
     */
    sharedData: {
        //This will be available in all views
        appName: 'Tethys Cloud',

        errors: (ctx) => ctx.session?.flashMessages.get('errors'),

        user_id: (ctx) => {
            return ctx.session?.flashMessages.get('user_id');
        },

        flash: (ctx) => {
            return {
                message: ctx.session?.flashMessages.get('message'),
                warning: ctx.session?.flashMessages.get('warning'),
                error: ctx.session?.flashMessages.get('error'),
            };
        },

        // params: ({ params }) => params,
        authUser: async ({ auth }: HttpContext) => {
            if (auth?.user) {
                await auth.user.load('roles');
                return auth.user;
                // {
                //     'id': auth.user.id,
                //     'login': auth.user.login,
                // };
            } else {
                return null;
            }
        },
    },

    /**
     * Options for the server-side rendering
     */
    ssr: {
        enabled: false,
        entrypoint: 'inertia/app/ssr.ts',
    },
});

// import { InertiaConfig } from '@ioc:EidelLev/Inertia';

// /*
// |--------------------------------------------------------------------------
// | Inertia-AdonisJS config
// |--------------------------------------------------------------------------
// |
// */

// export const inertia: InertiaConfig = {
//     view: 'app',
//     // ssr: {
//     //   enabled: false,
//     // },
// };
