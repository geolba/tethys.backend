const RolePermission: Object = {
    role_table: 'roles',
    permission_table: 'permissions',
    user_role_table: 'link_accounts_roles',
    user_permission_table: 'user_permission_table',
    role_permission_table: 'role_has_permissions',
};
export default RolePermission;
