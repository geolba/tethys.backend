/**
 * Config source: https://git.io/JfefZ
 *
 * Feel free to let us know via PR, if you find something broken in this config
 * file.
 */

import proxyAddr from 'proxy-addr';
import env from '#start/env';
// import app from '@adonisjs/core/services/app';
// import type { ProfilerConfig } from '@ioc:Adonis/Core/Profiler';
// import type { AssetsManagerConfig } from '@ioc:Adonis/Core/AssetsManager';
// import { ServerConfig } from "@adonisjs/core/services/server";
// import { LoggerConfig } from "@adonisjs/core/types/logger";
// import { ValidatorConfig } from "@adonisjs/validator/types";
import { defineConfig } from '@adonisjs/core/http';

/*
|--------------------------------------------------------------------------
| Application secret key
|--------------------------------------------------------------------------
|
| The secret to encrypt and sign different values in your application.
| Make sure to keep the `APP_KEY` as an environment variable and secure.
|
| Note: Changing the application key for an existing app will make all
| the cookies invalid and also the existing encrypted data will not
| be decrypted.
|
*/
export const appKey: string = env.get('APP_KEY');

/*
|--------------------------------------------------------------------------
| Http server configuration
|--------------------------------------------------------------------------
|
| The configuration for the HTTP(s) server. Make sure to go through all
| the config properties to make keep server secure.
|
*/
export const http = defineConfig({
    /*
|--------------------------------------------------------------------------
| Allow method spoofing
|--------------------------------------------------------------------------
|
| Method spoofing enables defining custom HTTP methods using a query string
| `_method`. This is usually required when you are making traditional
| form requests and wants to use HTTP verbs like `PUT`, `DELETE` and
| so on.
|
*/
    allowMethodSpoofing: false,

    /*
|--------------------------------------------------------------------------
| Subdomain offset
|--------------------------------------------------------------------------
*/
    subdomainOffset: 2,

    /*
|--------------------------------------------------------------------------
| Request Ids
|--------------------------------------------------------------------------
|
| Setting this value to `true` will generate a unique request id for each
| HTTP request and set it as `x-request-id` header.
|
*/
    generateRequestId: false,

    /*
|--------------------------------------------------------------------------
| Trusting proxy servers
|--------------------------------------------------------------------------
|
| Define the proxy servers that AdonisJs must trust for reading `X-Forwarded`
| headers.
|
*/
    trustProxy: proxyAddr.compile('loopback'),

    /*
|--------------------------------------------------------------------------
| Generating Etag
|--------------------------------------------------------------------------
|
| Whether or not to generate an etag for every response.
|
*/
    etag: false,

    /*
|--------------------------------------------------------------------------
| JSONP Callback
|--------------------------------------------------------------------------
*/
    jsonpCallbackName: 'callback',

    /*
|--------------------------------------------------------------------------
| Cookie settings
|--------------------------------------------------------------------------
*/
    cookie: {
        domain: '',
        path: '/',
        maxAge: '2h',
        httpOnly: true,
        secure: false,
        sameSite: false,
    },
});

/*
|--------------------------------------------------------------------------
| Profiler
|--------------------------------------------------------------------------
*/
// export const profiler: ProfilerConfig = {
//   /*
// |--------------------------------------------------------------------------
// | Toggle profiler
// |--------------------------------------------------------------------------
// |
// | Enable or disable profiler
// |
// */
//   enabled: true,

//   /*
// |--------------------------------------------------------------------------
// | Blacklist actions/row labels
// |--------------------------------------------------------------------------
// |
// | Define an array of actions or row labels that you want to disable from
// | getting profiled.
// |
// */
//   blacklist: [],

//   /*
// |--------------------------------------------------------------------------
// | Whitelist actions/row labels
// |--------------------------------------------------------------------------
// |
// | Define an array of actions or row labels that you want to whitelist for
// | the profiler. When whitelist is defined, then `blacklist` is ignored.
// |
// */
//   whitelist: [],
// };

/*
|--------------------------------------------------------------------------
| Validator
|--------------------------------------------------------------------------
|
| Configure the global configuration for the validator. Here's the reference
| to the default config https://git.io/JT0WE
|
*/
export const validator = {};

/*
|--------------------------------------------------------------------------
| Assets
|--------------------------------------------------------------------------
|
| Configure the asset manager you are using to compile the frontend assets
|
*/
// export const assets: AssetsManagerConfig = {
//   /*
// |--------------------------------------------------------------------------
// | Driver
// |--------------------------------------------------------------------------
// |
// | Currently we only support webpack encore and may introduce more drivers
// | in the future
// |
// */
//   driver: env.get('ASSETS_DRIVER'),

//   /*
// |--------------------------------------------------------------------------
// | Public path
// |--------------------------------------------------------------------------
// |
// | Directory to search for the "manifest.json" and the "entrypoints.json"
// | files
// |
// */
//   publicPath: app.publicPath('assets'),

//   /*
// |--------------------------------------------------------------------------
// | Script tag
// |--------------------------------------------------------------------------
// |
// | Define attributes for the entryPointScripts tags
// |
// */
//   script: {
//     attributes: {
//       defer: true,
//     },
//   },

//   /*
// |--------------------------------------------------------------------------
// | Style tag
// |--------------------------------------------------------------------------
// |
// | Define attributes for the entryPointStyles tags
// |
// */
//   style: {
//     attributes: {},
//   },
// };
