import { defineConfig } from '@adonisjs/auth';
import { Authenticators } from '@adonisjs/auth/types';
import { sessionGuard, sessionUserProvider } from '@adonisjs/auth/session';
// import User from '#models/user';
// import { SessionLucidUserProviderOptions } from '@adonisjs/auth/types/session';
import { Authenticator } from '@adonisjs/auth';
import { GuardFactory } from '@adonisjs/auth/types';

// export declare function sessionUserProvider<Model extends LucidAuthenticatable>(config: SessionLucidUserProviderOptions<Model>): SessionLucidUserProvider<Model>;

const authConfig = defineConfig({
    default: 'web',
    guards: {
        web: sessionGuard({
            useRememberMeTokens: false,
            provider: sessionUserProvider({
                model: () => import('#models/user'),
            }),
        }),
    },
});

export default authConfig;

/**
 * Inferring types from the configured auth
 * guards.
 */
declare module '@adonisjs/auth/types' {
    // export type InferAuthenticators<
    //     Config extends ConfigProvider<{
    //         default: unknown;
    //         guards: unknown;
    //     }>,
    // > = Awaited<ReturnType<Config['resolver']>>['guards'];
    // interface ProvidersList {
    //     /*
    //         |--------------------------------------------------------------------------
    //         | User Provider
    //         |--------------------------------------------------------------------------
    //         |
    //         | The following provider uses Lucid models as a driver for fetching user
    //         | details from the database for authentication.
    //         |
    //         | You can create multiple providers using the same underlying driver with
    //         | different Lucid models.
    //         |
    //         */
    //     // user: {
    //     //     implementation: SessionLucidUserProvider<typeof User>;
    //     //     config: LucidProviderConfig<typeof User>;
    //     // };
    //     user: {
    //         implementation: SessionLucidUserProvider<typeof User>;
    //         config: SessionLucidUserProviderOptions<typeof User>;
    //     };
    // }

    interface Authenticators extends InferAuthenticators<typeof authConfig> {}

    //   const PROVIDER_REAL_USER: unique symbol;
    //   export type SessionGuardUser<RealUser> = {
    //     getId(): string | number | BigInt;
    //     getOriginal(): RealUser;
    // };

    //   export interface SessionUserProviderContract<User> {
    //     [PROVIDER_REAL_USER]: User;
    //     /**
    //      * Create a user object that acts as an adapter between
    //      * the guard and real user value.
    //      */
    //     createUserForGuard(user: User): Promise<SessionGuardUser<User>>;
    //     /**
    //      * Find a user by their id.
    //      */
    //     findById(identifier: string | number | BigInt): Promise<SessionGuardUser<User> | null>;
    // }
}

// declare module '@adonisjs/core/types' {
//     interface EventsList extends InferAuthEvents<Authenticators> {}
// }

declare module '@adonisjs/core/http' {
    interface HttpContext {
        auth: Authenticator<Authenticators extends Record<string, GuardFactory> ? Authenticators : never>;
    }
}
