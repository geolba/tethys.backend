import env from '#start/env';

interface OaiConfig {
    max: { listidentifiers: number; listrecords: number };
    workspacePath: string;
    redis: { ttl: number };
}
const config: OaiConfig = {
    max: {
        listidentifiers: env.get('OAI_LIST_SIZE', 100),
        listrecords: env.get('OAI_LIST_SIZE', 100),
    },
    workspacePath: 'workspace',
    redis: {
        ttl: 86400, //sec 1 day
    },
};
export default config;
