/*
|--------------------------------------------------------------------------
| Preloaded File - node ace make:preload validator
|--------------------------------------------------------------------------
|
| Any code written inside this file will be executed during the application
| boot.
https://issuehunt.io/r/adonisjs/validator/issues/84
|
*/
import vine from '@vinejs/vine';
// import { FieldContext } from '@vinejs/vine/types';
// import db from '@adonisjs/lucid/services/db';
import { VanillaErrorReporter } from '#validators/vanilla_error_reporter';


// vine.messagesProvider = new SimpleMessagesProvider({
//      // Applicable for all fields
//      'required': 'The {{ field }} field is required',
//      'string': 'The value of {{ field }} field must be a string',
//      'email': 'The value is not a valid email address',
   
//      // 'contacts.0.email.required': 'The primary email of the contact is required',
//      // 'contacts.*.email.required': 'Contact email is required',
//      'permissions.minLength': 'at least {{ options.minLength }} permission must be defined',
//      'permissions.*.number': 'Define permissions as valid numbers',
//   })
vine.errorReporter = () => new VanillaErrorReporter();
  

// /**
//  * Options accepted by the unique rule
//  */
// type Options = {
//     table: string;
//     column: string;
// };

// /**
//  * Implementation
//  */
// async function unique(value: unknown, options: Options, field: FieldContext) {
//     /**
//      * We do not want to deal with non-string
//      * values. The "string" rule will handle the
//      * the validation.
//      */
//     if (typeof value !== 'string') {
//         return;
//     }

//     const builder = await db
//     .from(options.table)
//     .select(options.column)
//     .where(options.column, value).first();
//     const row = await builder.first();

//     if (row) {
//         field.report('The {{ field }} field is not unique', 'unique', field);
//     }
// }

// /**
//  * Converting a function to a VineJS rule
//  */
// export const uniqueRule = vine.createRule(unique);

// VineString.macro('unique', function (this: VineString, options: Options) {
//     return this.use(uniqueRule(options));
// });

// // declare module '@vinejs/vine' {
// //     interface VineString {
// //         unique(options: Options): this;
// //     }
// // }
