/*
|--------------------------------------------------------------------------
| Application middleware
|--------------------------------------------------------------------------
|
| This file is used to define middleware for HTTP requests. You can register
| middleware as a `closure` or an IoC container binding. The bindings are
| preferred, since they keep this file clean.
|
*/

import server from '@adonisjs/core/services/server';
import router from '@adonisjs/core/services/router';

server.errorHandler(() => import('#exceptions/handler'));

/**
 * The server middleware stack runs middleware on all the HTTP
 * requests, even if there is no route registered for
 * the request URL.
 */
server.use([
    () => import('#middleware/container_bindings_middleware'),
    () => import('@adonisjs/static/static_middleware'),
    // () => import('@adonisjs/cors/cors_middleware'),
    () => import('@adonisjs/inertia/inertia_middleware'),
]);

/**
 * The router middleware stack runs middleware on all the HTTP
 * requests with a registered route.
 */
router.use([
    () => import('@adonisjs/core/bodyparser_middleware'),
    () => import('@adonisjs/session/session_middleware'),
    () => import('@adonisjs/shield/shield_middleware'),
    // () => import('@adonisjs/inertia/inertia_middleware'),
    () => import('@adonisjs/auth/initialize_auth_middleware'),
    () => import('#middleware/stardust_middleware'),
    () => import('#middleware/normalize_newlines_middleware'),
]);

/**
 * Named middleware collection must be explicitly assigned to
 * the routes or the routes group.
 */
export const middleware = router.named({
    guest: () => import('#middleware/guest_middleware'),
    auth: () => import('#middleware/auth_middleware'),
    is: () => import('#middleware/role_middleware'),
    can: () => import('#middleware/can_middleware'),
});
