/*
|--------------------------------------------------------------------------
| Preloaded File - node ace make:preload rules/fileLength
|--------------------------------------------------------------------------
|*/

import { FieldContext } from '@vinejs/vine/types';
import vine from '@vinejs/vine';
// import { VineString } from '@vinejs/vine';
import { VineMultipartFile, isBodyParserFile } from '#providers/vinejs_provider';
import type { MultipartFile } from '@adonisjs/core/bodyparser';

/**
 * Options accepted by the unique rule
 */
// type Options = {
//     mainLanguageField: string;
//     typeField: string;
// };
type Options = {
    // size: string | number;
    // extnames: string[];
    clientNameSizeLimit: number
};

async function filenameLength(file: VineMultipartFile | unknown, options: Options, field: FieldContext) {
    // if (typeof value !== 'string' && typeof value != 'number') {
    //     return;
    // }
    if (!isBodyParserFile(file)) {
        return;
    }
    const validatedFile = file as MultipartFile;

    if (validatedFile.clientName.length > options.clientNameSizeLimit) {
      
        field.report(`Filename length should be less or equal than ${options.clientNameSizeLimit} characters`, 'filenameLength', field);
    }   
}

export const filenameLengthRule = vine.createRule(filenameLength);

declare module '#providers/vinejs_provider' {
    interface VineMultipartFile {
        filenameLength(options: Options): this;
    }
}

VineMultipartFile.macro('filenameLength', function (this: VineMultipartFile, options: Options) {
    return this.use(filenameLengthRule(options));
});
