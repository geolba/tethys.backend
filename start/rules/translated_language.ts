/*
|--------------------------------------------------------------------------
| Preloaded File - node ace make:preload rules/translatedLanguage
|--------------------------------------------------------------------------
|*/

import { FieldContext } from '@vinejs/vine/types';
import vine from '@vinejs/vine';
import { VineString } from '@vinejs/vine';

/**
 * Options accepted by the unique rule
 */
type Options = {
    mainLanguageField: string;
    typeField: string;
};

async function translatedLanguage(value: unknown, options: Options, field: FieldContext) {
    if (typeof value !== 'string' && typeof value != 'number') {
        return;
    }

    // const type = validator.helpers.getFieldValue(typeField, root, tip); //'type' = 'Translated'
    const typeValue = vine.helpers.getNestedValue(options.typeField, field); //'Main' or 'Translated'

    // const mainLanguage = validator.helpers.getFieldValue(mainLanguageField, root, tip);
    const mainLanguage = field.data[options.mainLanguageField]; // 'en' or 'de'

    if (typeValue === 'Translated') {
        if (value === mainLanguage) {
            // report thattranlated language field is same as main language field of dataset
            field.report('The tranlated {{ field }} hast the same language as dataset language', 'translatedLanguage', field);
        }
    }
}

export const translatedLanguageRule = vine.createRule(translatedLanguage);

declare module '@vinejs/vine' {
    interface VineString {
        translatedLanguage(options: Options): this;
    }
}

VineString.macro('translatedLanguage', function (this: VineString, options: Options) {
    return this.use(translatedLanguageRule(options));
});
