import router from '@adonisjs/core/services/router';

import AuthorsController from '#controllers/Http/Api/AuthorsController';
import DatasetController from '#controllers/Http/Api/DatasetController';
import HomeController from '#controllers/Http/Api/HomeController';
import FileController from '#controllers/Http/Api/FileController';
import AvatarController from '#controllers/Http/Api/AvatarController';
import UserController from '#controllers/Http/Api/UserController';
import { middleware } from '../kernel.js'
// API
router.group(() => {
  

   
        router.get('authors', [AuthorsController, "index"]).as('author.index');
        router.get('datasets', [DatasetController, "index"]).as('dataset.index');
        router.get('persons', [AuthorsController, "persons"]).as('author.persons');
     
        router.get('/dataset',  [DatasetController, "findAll"]).as('dataset.findAll');
        router.get('/dataset/:publish_id', [DatasetController, "findOne"]).as('dataset.findOne');
        router.get('/sitelinks/:year', [HomeController, "findDocumentsPerYear"]);
        router.get('/years',  [HomeController, "findYears"]);
        router.get('/statistic/:year',  [HomeController, "findPublicationsPerMonth"]);

        router.get('/download/:id', [FileController, "findOne"]).as('file.findOne');

        router.get('/avatar/:name/:background?/:textColor?/:size?', [AvatarController, 'generateAvatar']);

    
        router.post('/twofactor_totp/settings/enable/:state/:code?', [UserController, 'enable']).as('apps.twofactor_totp.enable') .use(middleware.auth());
        router.post('/twofactor_backupcodes/settings/create', [UserController, 'createCodes']).as('apps.twofactor_backupcodes.create') .use(middleware.auth());
    
})
    // .namespace('App/Controllers/Http/Api')
    .prefix('api');
