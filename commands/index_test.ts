import { BaseCommand } from '@adonisjs/core/ace';
import type { CommandOptions } from '@adonisjs/core/types/ace';

export default class IndexTest extends BaseCommand {
    static commandName = 'index:test';
    static description = '';

    static options: CommandOptions = {};

    async run() {
        this.logger.info('Hello world from "IndexTest"');
    }
}
